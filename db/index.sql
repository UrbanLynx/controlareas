USE [MyViFlow.Database]
GO

/****** Object:  Index [Index_Time_Id]    Script Date: 20.05.2015 16:40:51 ******/
CREATE NONCLUSTERED INDEX [Index_Time_Id2] ON [dbo].[Event]
(
	[Time] desc,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


