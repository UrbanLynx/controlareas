USE [MyViFlow.Database]

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT Event.Id, Event.PlateId, Plate.Recognized, RealEvent.RealVehicleId, RealVehicle.PlateId as RV_plateId,  CameraArea.CameraId, CameraArea.FromAreaId, CameraArea.ToAreaId
  FROM Event
  INNER JOIN RealEvent
  ON Event.Id=RealEvent.EventId
  INNER JOIN RealVehicle
  On RealEvent.RealVehicleId=RealVehicle.Id
  INNER JOIN Plate
  On Event.PlateId=Plate.Id
  INNER JOIN CameraArea
  On Event.CameraId=CameraArea.CameraId
  ORDER BY Event.Id
GO

SELECT RealVehicle.Id, RealVehicle.PlateId, Plate.Recognized
  FROM RealVehicle
  INNER JOIN Plate
  On RealVehicle.PlateId=Plate.Id
  ORDER BY RealVehicle.Id
GO

