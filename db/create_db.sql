USE [ViFlow.Database]
GO

CREATE TABLE [dbo].[ControlArea] (
    [Id]             INT             NOT NULL,
    [CreationTime]   DATETIME        NOT NULL,
    [IsExternal]     BIT        NOT NULL,
    CONSTRAINT [PK_ControlArea] PRIMARY KEY ([Id])
);

CREATE TABLE [dbo].[CameraArea] (
    [Id]             BIGINT          NOT NULL,
    [CameraId]       INT             NOT NULL,
    [FromAreaId]     INT             NOT NULL,
    [ToAreaId]       INT             NOT NULL,
    CONSTRAINT [PK_CameraArea] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CameraArea_Camera_CameraId] FOREIGN KEY ([CameraId]) REFERENCES [dbo].[Camera] ([Id]),
    CONSTRAINT [FK_CameraArea_ControlArea_FromAreaId] FOREIGN KEY ([FromAreaId]) REFERENCES [dbo].[ControlArea] ([Id]),
    CONSTRAINT [FK_CameraArea_ControlArea_ToAreaId] FOREIGN KEY ([ToAreaId]) REFERENCES [dbo].[ControlArea] ([Id])
);

CREATE TABLE [dbo].[RegionArea] (
    [Id]             BIGINT          NOT NULL,
    [AreaId]       INT             NOT NULL,
    [RegionId]     INT             NOT NULL,
    CONSTRAINT [PK_RegionArea] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RegionArea_Area_AreaId] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([Id]),
    CONSTRAINT [FK_RegionArea_Region_RegionId] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);


CREATE TABLE [dbo].[Vehicle] (
    [Id]             INT          NOT NULL,
    [PlateId]        INT             NOT NULL,
    [Brand]          INT             NOT NULL
    CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vehicle_Plate_PlateId] FOREIGN KEY ([PlateId]) REFERENCES [dbo].[Plate] ([Id])
);

CREATE TABLE [dbo].[WantedVehicle] (
    [Id]             INT          NOT NULL,
    [VehicleId]      INT             NOT NULL,
    [Reason]         VARCHAR (MAX)             NULL,
    CONSTRAINT [PK_WantedVehicle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WantedVehicle_Vehicle_VehicleId] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([Id])
);


CREATE TABLE [dbo].[AreaMovement] (
    [Id]             BIGINT          NOT NULL,
    [VehicleId]      INT             NOT NULL,
    [ControlAreaId]  INT             NOT NULL,
    [FromEventId]    BIGINT          NULL,
    [ToEventId]      BIGINT          NULL,
    CONSTRAINT [PK_AreaMovement] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AreaMovement_Vehicle_VehicleId] FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([Id]),
    CONSTRAINT [FK_AreaMovement_ControlArea_ControlAreaId] FOREIGN KEY ([ControlAreaId]) REFERENCES [dbo].[ControlArea] ([Id]),
    CONSTRAINT [FK_AreaMovement_Event_FromEventId] FOREIGN KEY ([FromEventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_AreaMovement_Event_ToEventId] FOREIGN KEY ([ToEventId]) REFERENCES [dbo].[Event] ([Id])
);

GO



