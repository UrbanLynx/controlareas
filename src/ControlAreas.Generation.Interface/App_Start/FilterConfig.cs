﻿using System.Web;
using System.Web.Mvc;

namespace ControlAreas.Generation.Interface
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
