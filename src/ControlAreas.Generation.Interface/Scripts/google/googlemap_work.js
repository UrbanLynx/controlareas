﻿
var map;
var markers = [];
var currentCA = [];
var currentGoogleCA = [];

var markerCounter = 0;
var areaCounter = 0;

var mapModel = {
    cameras: [],
    controlAreas: [],
    regionNumber: 0,
    brandNumber: 0,
    plateNumber: 0,
    officialPlates: 0
}

var eventModel = {
    eventNumber: 0,
    plateId: 0,
    cameraId: 0,
    sleepTime: 10000
}

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(55.74180, 37.59383),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"),
        mapOptions);

    google.maps.event.addListener(map, 'click', function (event) {

        var marker = new google.maps.Marker({
            position: event.latLng,
            map: map,
            title: markerCounter.toString()
        });

        mapModel.cameras.push({
            id: markerCounter,
            latitude: parseFloat(event.latLng.lat()),
            longitude: parseFloat(event.latLng.lng())
        });

        markerCounter += 1;

        google.maps.event.addListener(marker, 'click', function() {
            currentCA.push(parseInt(marker.title));
            currentGoogleCA.push(marker.position);
        });
        markers.push(marker);

        console.log('Lat: ' + event.latLng.lat() + ' Lng: ' + event.latLng.lng());
        //console.log(marker.lat(), marker.lng());
    });
}

function drawPolygon(polygonPoints) {
    var caPolygon = new google.maps.Polygon({
        paths: polygonPoints,
        strokeColor: '#002560',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#AF0500',
        fillOpacity: 0.35
    });

    caPolygon.setMap(map);
}

function nextCA() {
    mapModel.controlAreas.push({
        id: areaCounter,
        positions: currentCA,
        isExternal: $('#isExternal').is(':checked')
    });
    areaCounter += 1;
    currentCA = [];

    if (currentGoogleCA.length > 1) {
        currentGoogleCA.push(currentGoogleCA[0]);
        drawPolygon(currentGoogleCA);
    }
    currentGoogleCA = [];
}

function getParameters() {
    mapModel.regionNumber = parseInt($('#regionNumber').val());
    mapModel.brandNumber = parseInt($('#brandNumber').val());
    mapModel.plateNumber = parseInt($('#plateNumber').val());
    mapModel.officialPlates = parseInt($('#officialPlates').val());
}

function submitMap() {
    getParameters();
    $.ajax({
        url: "Map/SubmitParameters",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(mapModel)/*,
        success: ajaxOnSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }*/
    });
}

function clearCA() {
    currentCA = [];
}

function generateFromFile() {
    $.ajax({
        url: "Map/GenerateFromFile",
        type: 'GET',
        contentType: 'application/json',
        success: function(data) {
            alert('БД создана из файла успешно!');
        }
    });
}

function clearDb() {
    $.ajax({
        url: "Map/ClearDb"
    });
}

function generateEventsFinite() {
    getEventParameters();
    $.ajax({
        url: "Map/GenerateEventsFinite",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(eventModel)
    });
}

function startGenerateEvents() {
    $.ajax({
        url: "Map/StartGenerateEvents",
        type: 'GET',
        contentType: 'application/json'
    });
}

function stopGenerateEvents() {
    $.ajax({
        url: "Map/StopGenerateEvents",
        type: 'GET',
        contentType: 'application/json'
    });
}

function getEventParameters() {
    eventModel.eventNumber = parseInt($('#eventNumber').val());
    eventModel.plateId = parseInt($('#eventPlateId').val());
    eventModel.cameraId = parseInt($('#eventCameraId').val());
    eventModel.sleepTime = parseInt($('#eventSleepTime').val());
}

initialize();