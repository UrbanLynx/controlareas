﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ControlAreas.Generation.Entities;
using ControlAreas.Generation.Interface.Models;

namespace ControlAreas.Generation.Interface.Controllers
{
    public class MapController : Controller
    {
        private StructureGeneration structureGenerator = new StructureGeneration();
        private EventGeneration eventGenerator;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitParameters(MapModel model)
        {
            structureGenerator.GenerateModel(model);
            return null;
        }

        public ActionResult GenerateFromFile()
        {
            structureGenerator.GenerateModelFromFile();
            return null;
        }

        public ActionResult ClearDb()
        {
            structureGenerator.DeleteDatabase();
            return null;
        }


        public ActionResult GenerateEventsFinite(EventModel model)
        {
            //var eventGenerator = new EventGeneration(model);
            //Task.Factory.StartNew(eventGenerator.GenerateEventsFinite);
            return null;
        }

        public ActionResult StartGenerateEvents()
        {
            // TODO: добавить создание задачи с токеном или еще как то, чтобы можно было остановить после запуска
            var model = new EventModel();
            eventGenerator = new EventGeneration(model);
            eventGenerator.StartGeneration();
            return null;
        }

        public ActionResult StopGenerateEvents()
        {
            // отменять CancellationToken
            if (eventGenerator != null)
            {
                eventGenerator.StopGeneration();
            }
            return null;
        }

        /*public ActionResult SubmitData(MapModel model)
        {
            return View();
        }*/
    }
}