﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ControlAreas.Generation.Interface.Startup))]
namespace ControlAreas.Generation.Interface
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
