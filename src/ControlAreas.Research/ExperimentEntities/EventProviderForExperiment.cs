﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Adapter.Services;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Generation;
using ControlAreas.Research.Public.ExperimentEntities;

namespace ControlAreas.Research.ExperimentEntities
{
    class EventProviderForExperiment: EventProvider
    {
        private ExperimentSettings _experimentSettings;
        private Random _random = new Random(DateTime.Now.Millisecond);
        private DbInteraction db = new DbInteraction();

        public EventProviderForExperiment(ViFlowContext viFlowContext, ExperimentSettings experimentSettings) : base(viFlowContext)
        {
            _experimentSettings = experimentSettings;
        }

        

        public void ProcessOldEvents(ExperimentSettings experimentSettings)
        {
            var events = _dbs.EventDb.GetOldEvents(0);
            foreach (var @event in events)
            {
                //_eventProcessor(ChangeEvent(@event));
            }
        }

        /*public Event ChangeEvent(Event curEvent)
        {
            var newEvent = new Event
            {
                Id = curEvent.
            }
        }*/

        private int? GetPlate(RealVehicle rv, bool changePlate)
        {
            var plate = rv.Plate;
            if (changePlate)
            {
                var possibleChanges = new List<KeyValuePair<Action, double>>
                {
                    new KeyValuePair<Action, double>(() => plate = db.ChooseUnrealPlate(),_experimentSettings.RecognitionError),
                    new KeyValuePair<Action, double>(() => plate = db.MakeNotValid(plate),_experimentSettings.SkipError),
                    new KeyValuePair<Action, double>(() => plate = plate, 1)
                };
                ExecuteRandom(possibleChanges);
                plate = db.AddPlateIfNeeded(plate);
            }
            return plate.Id; // проверить, что после добавления в БД есть Id
        }

        private void ExecuteRandom(List<KeyValuePair<Action, double>> elements)
        {
            double diceRoll = _random.NextDouble();

            double cumulative = 0.0;
            for (int i = 0; i < elements.Count; i++)
            {
                cumulative += elements[i].Value;
                if (diceRoll < cumulative)
                {
                    elements[i].Key();
                    break;
                }
            }
        }
    }
}
