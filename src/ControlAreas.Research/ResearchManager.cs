﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlAreas.Adapter;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.EventProcessing;
using ControlAreas.Generation;
using ControlAreas.Research.ExperimentEntities;
using ControlAreas.Research.Public;
using ControlAreas.Research.Public.ExperimentEntities;

namespace ControlAreas.Research
{
    class ResearchManager
    {
        private List<ExperimentResult> _experimentResults = new List<ExperimentResult>();

        public void MakeRecognitionSkipExperiments()
        {
            var experimentSettings = new ExperimentSettings()
            {
                EventsN = 100,
                RecognitionError = 0.0,
                SkipError = 0.0,
                RealVehiclesN = 20,
                CameraIds = new List<int> { 3}
            };
            /*var experimentSettings = new ExperimentSettings()
            {
                EventsN = 10,
                RecognitionError = 0.0,
                SkipError = 0.0,
                RealVehiclesN = 1,
                CameraIds = 3
            };*/
            double recognitionErrorStep = 0.1;
            double skipErrorStep = 0.1;
            int experimentIndex = 0;
            var appendFileName = String.Format("Experiment_errors_append_{0:yyyy-MM-dd-HH-mm}.txt", DateTime.Now);
            WriteTitle(appendFileName);

            while (ContinueRecognition(experimentSettings))
            {
                while (ContinueSkip(experimentSettings))
                {
                    var timer = new Stopwatch();
                    timer.Start();


                    ResearchStatistics.NewExperiment(experimentSettings);
                    
                    var experimentGenerator = new ExperimentGeneration(experimentSettings);
                    experimentGenerator.GenerateFinite();

                    /*timer.Stop();
                    Console.WriteLine("Time of generating all evenst - {0}s", timer.ElapsedMilliseconds/1000);
                    timer.Restart();*/

                    ProcessEvents(experimentSettings);
                    ClearDb();

                    timer.Stop();
                    Console.WriteLine("Experiment errors {0}, {1}s: recognition - {2}; skip - {3}", experimentIndex++,
                        timer.ElapsedMilliseconds/1000, experimentSettings.RecognitionError, experimentSettings.SkipError);

                    var result = ResearchStatistics.GetExperimentResults();
                    _experimentResults.Add(result);
                    AppendToFile(result, appendFileName);

                    experimentSettings = SetSkipError(experimentSettings, skipErrorStep);;
                }
                experimentSettings = SetRecognitionError(experimentSettings, recognitionErrorStep);
            }

            var filename = String.Format("Experiment_errors_{0:yyyy-MM-dd-HH-mm}.txt", DateTime.Now);
            WriteResultsToFile(_experimentResults, filename);
        }

        public void MakeCameraNumberExperiments()
        {
            var camerasId = new List<int> { 1, 2, 3, 4, 7, 8, 9, 10, 13, 14, 15, 16 }; // камеры, связанные с внутренней зоной

            var experimentSettingsStart = new ExperimentSettings()
            {
                EventsN = 100,
                RecognitionError = 0.0,
                SkipError = 0.2, // фиксируем % пропуска и меняем % распознавания
                RealVehiclesN = 20,
                CameraIds = new List<int>() 
            };

            double recognitionErrorStep = 0.2;
            int experimentIndex = 0;

            var uniqueName = "number_rec";
            var appendFileName = String.Format("Experiment_{0}_append_{1:yyyy-MM-dd-HH-mm}.txt", uniqueName, DateTime.Now);
            WriteTitle(appendFileName);

            var experimentSettings = CopySettings(experimentSettingsStart);
            while (ContinueRecognition(experimentSettings))
            {
                for (int i = 1; i <= camerasId.Count; i++)
                {
                    experimentSettings = CopySettings(experimentSettings);
                    experimentSettings.CameraIds = camerasId.Take(i).ToList();
                    experimentSettings.EventsN = experimentSettingsStart.EventsN * i;
                
                    var timer = new Stopwatch();
                    timer.Start();

                    ResearchStatistics.NewExperiment(experimentSettings);

                    var experimentGenerator = new ExperimentGeneration(experimentSettings);
                    experimentGenerator.GenerateFinite();

                    ProcessEvents(experimentSettings);
                    ClearDb();

                    timer.Stop();
                    Console.WriteLine("Experiment number {0}, {1}s: recognition - {2}; skip - {3}; cameras - {4}",
                        experimentIndex++, timer.ElapsedMilliseconds/1000, experimentSettings.RecognitionError,
                        experimentSettings.SkipError, experimentSettings.CameraIds.Count);

                    var result = ResearchStatistics.GetExperimentResults();
                    _experimentResults.Add(result);
                    AppendToFile(result, appendFileName);
                }

                experimentSettings = CopySettings(experimentSettings);
                experimentSettings.RecognitionError += recognitionErrorStep;
            }

            var filename = String.Format("Experiment_number_{0:yyyy-MM-dd-HH-mm}.txt", DateTime.Now);
            WriteResultsToFile(_experimentResults, filename);

            /*for (int i = 1; i <= camerasId.Count; i++)
            {
                var experimentSettings = CopySettings(experimentSettingsStart);
                experimentSettings.CameraIds = camerasId.Take(i).ToList();

                while (ContinueRecognition(experimentSettings))
                {
                    var timer = new Stopwatch();
                    timer.Start();

                    ResearchStatistics.NewExperiment(experimentSettings);

                    var experimentGenerator = new ExperimentGeneration(experimentSettings);
                    experimentGenerator.GenerateFinite();

                    ProcessEvents(experimentSettings);
                    ClearDb();

                    timer.Stop();
                    Console.WriteLine("Experiment number {0}, {1}s: recognition - {2}; skip - {3}", experimentIndex++,
                        timer.ElapsedMilliseconds / 1000, experimentSettings.RecognitionError, experimentSettings.SkipError);

                    var result = ResearchStatistics.GetExperimentResults();
                    _experimentResults.Add(result);
                    AppendToFile(result, appendFileName);

                    experimentSettings = CopySettings(experimentSettings);
                    experimentSettings.RecognitionError += recognitionErrorStep;
                }
            }*/

            
        }

        public void MakeCameraNumberSkipExperiments()
        {
            var camerasId = new List<int> { 1, 2, 3, 4, 7, 8, 9, 10, 13, 14, 15, 16 }; // камеры, связанные с внутренней зоной

            var experimentSettingsStart = new ExperimentSettings()
            {
                EventsN = 100,
                RecognitionError = 0.2,
                SkipError = 0.0, // фиксируем % пропуска и меняем % распознавания
                RealVehiclesN = 20,
                CameraIds = new List<int>()
            };

            double skipErrorStep = 0.2;
            int experimentIndex = 0;

            var uniqueName = "number_skip";
            var appendFileName = String.Format("Experiment_{0}_append_{1:yyyy-MM-dd-HH-mm}.txt",uniqueName, DateTime.Now);
            WriteTitle(appendFileName);

            var experimentSettings = CopySettings(experimentSettingsStart);
            while (ContinueSkip(experimentSettings))
            {
                for (int i = 1; i <= camerasId.Count; i++)
                {
                    experimentSettings = CopySettings(experimentSettings);
                    experimentSettings.CameraIds = camerasId.Take(i).ToList();
                    experimentSettings.EventsN = experimentSettingsStart.EventsN * i;

                    var timer = new Stopwatch();
                    timer.Start();

                    ResearchStatistics.NewExperiment(experimentSettings);

                    var experimentGenerator = new ExperimentGeneration(experimentSettings);
                    experimentGenerator.GenerateFinite();

                    ProcessEvents(experimentSettings);
                    ClearDb();

                    timer.Stop();
                    Console.WriteLine("Experiment number {0}, {1}s: recognition - {2}; skip - {3}; cameras - {4}",
                        experimentIndex++, timer.ElapsedMilliseconds / 1000, experimentSettings.RecognitionError,
                        experimentSettings.SkipError, experimentSettings.CameraIds.Count);

                    var result = ResearchStatistics.GetExperimentResults();
                    _experimentResults.Add(result);
                    AppendToFile(result, appendFileName);
                }

                experimentSettings = CopySettings(experimentSettings);
                experimentSettings.SkipError += skipErrorStep;
            }

            var filename = String.Format("Experiment_{0}_{1:yyyy-MM-dd-HH-mm}.txt", uniqueName, DateTime.Now);
            WriteResultsToFile(_experimentResults, filename);
        }

        private void ClearDb()
        {
            var db = new ViFlowContext();
            var commandList = new List<string>
            {
                "DELETE FROM WantedVehicle",
                "DELETE FROM AreaMovement",
                "DELETE FROM Vehicle",
                "DELETE FROM RealEvent",
                "DELETE FROM RealVehicle",
                "DELETE FROM OfficialPlate",
                "DELETE FROM Event",
                "DELETE FROM Plate",
            };

            foreach (var command in commandList)
            {
                db.Database.ExecuteSqlCommand(command);
            }
            db.SaveChanges();
        }

        private ExperimentSettings SetRecognitionError(ExperimentSettings experimentSettings, double recognitionErrorStep)
        {
            var copy = CopySettings(experimentSettings);
            copy.SkipError = 0;
            copy.RecognitionError += recognitionErrorStep;
            return copy;
        }

        private ExperimentSettings SetSkipError(ExperimentSettings experimentSettings, double skipErrorStep)
        {
            var copy = CopySettings(experimentSettings);
            copy.SkipError += skipErrorStep;
            return copy;
        }

        private ExperimentSettings CopySettings(ExperimentSettings experimentSettings)
        {
            return new ExperimentSettings
            {
                CameraIds = experimentSettings.CameraIds,
                EventsN = experimentSettings.EventsN,
                RealVehiclesN = experimentSettings.RealVehiclesN,
                RecognitionError = experimentSettings.RecognitionError,
                SkipError = experimentSettings.SkipError
            };
        }

        private bool ContinueSkip(ExperimentSettings experimentSettings)
        {
            return experimentSettings.SkipError <= 1 - experimentSettings.RecognitionError;
        }

        private bool ContinueRecognition(ExperimentSettings experimentSettings)
        {
            return experimentSettings.RecognitionError <= 1 - experimentSettings.SkipError;
        }

        private void ProcessEvents(ExperimentSettings experimentSettings)
        {
            var db = new ViFlowContext();
            //var eventProvider = new EventProviderForExperiment(db, experimentSettings);
            //var adapter = new AdapterManager(db, eventProvider);
            var adapter = new AdapterManager(db);
            var events = new EventManager(db);

            adapter.EventProvider.AddEventProcessor(events.ProcessEvent);
            adapter.EventProvider.ProcessOldEvents();
        }

        private void AppendToFile(ExperimentResult result, string filename)
        {
            using (StreamWriter writer = File.AppendText(filename))
            {
                WriteLine(writer,result);
            }
        }

        private void WriteResultsToFile(IEnumerable<ExperimentResult> results, string filename)
        {
            WriteTitle(filename);
            using (var writer = new StreamWriter(filename,true))
            {
                foreach (var result in results)
                {
                    WriteLine(writer,result);
                }
            }
        }

        private void WriteTitle(string filename)
        {
            using (var writer = new StreamWriter(filename, true))
            {
                writer.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}",
                    "Recognition",
                    "Skip",
                    "Cameras",
                    "pFirstAp",
                    "pNotStandart",
                    "pStandart",
                    "pFirstAndNotStandart",
                    "EventsGenerated",
                    "VehiclesGenerated",
                    "FirstAppearanceN",
                    "NotStandartN",
                    "StandartN"
                    );
            }
            
        }

        private void WriteLine(StreamWriter writer, ExperimentResult result)
        {
            double pFirstAppearance = (double)result.FirstAppearanceN / result.EventsGenerated;
            double pNotStandart = (double)result.NotStandartN / result.EventsGenerated;
            double pStandart = (double)result.StandartN / result.EventsGenerated;
            double pFirstAndNotStandart = (double)(result.FirstAppearanceN + result.NotStandartN) / result.EventsGenerated;

            writer.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}",
                result.ExperimentSettings.RecognitionError,
                result.ExperimentSettings.SkipError,
                result.ExperimentSettings.CameraIds.Count(),
                pFirstAppearance,
                pNotStandart,
                pStandart,
                pFirstAndNotStandart,
                result.EventsGenerated,
                result.VehiclesGenerated,
                result.FirstAppearanceN,
                result.NotStandartN,
                result.StandartN);
        }

    }
}
