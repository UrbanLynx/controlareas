﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAreas.Research
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose research:");
            Console.WriteLine("1 - Skip and Recognition error on one camera");
            Console.WriteLine("2 - Recognition error on many cameras");
            Console.WriteLine("3 - Skip error on many cameras");
            Console.WriteLine("4 - All above");
            var choice = Convert.ToInt32(Console.ReadLine());

            var manager = new ResearchManager();
            Console.WriteLine("Research started.");
            switch (choice)
            {
                case 1: 
                    manager.MakeRecognitionSkipExperiments();
                    break;
                case 2: 
                    manager.MakeCameraNumberExperiments();
                    break;
                case 3: 
                    manager.MakeCameraNumberSkipExperiments();
                    break;
                case 4: 
                    manager.MakeRecognitionSkipExperiments(); 
                    manager.MakeCameraNumberExperiments();
                    manager.MakeCameraNumberSkipExperiments();
                    break;
            }
            
            Console.WriteLine("Research finished.");
            Console.ReadLine();
        }
    }
}
