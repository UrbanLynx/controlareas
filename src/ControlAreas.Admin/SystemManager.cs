﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlAreas.Adapter;
using ControlAreas.Dbs;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.EventProcessing;

namespace ControlAreas.Admin
{
    class SystemManager
    {
        private AdapterManager _adapter;
        private EventManager _event;

        public SystemManager()
        {
            var db = new ViFlowContext();
            _adapter = new AdapterManager(db);
            _event = new EventManager(db);
        }

        public void InitializeComponents()
        {
            _adapter.EventProvider.AddEventProcessor(_event.ProcessEvent);
        }
        public void StartMonitoring()
        {
            //Task.Factory.StartNew(() => _adapter.EventProvider.StartMonitorEvents(),new CancellationToken());
            _adapter.EventProvider.ProcessOldEvents();
            //Task.Factory.StartNew(() => _adapter.EventProvider.StartMonitorEventsByIds(), new CancellationToken());
        }

        public void StopMonitoring()
        {
            _adapter.EventProvider.StopMonitorEvents();
        } 
    }
}
