﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControlAreas.Admin
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Program started");
            var manager = new SystemManager();
            manager.InitializeComponents();
            manager.StartMonitoring();
            Console.WriteLine("Event monitoring sarted. To Cancel press Enter...");
            Console.ReadLine();
            manager.StopMonitoring();
            Thread.Sleep(3000);
        }
    }
}
