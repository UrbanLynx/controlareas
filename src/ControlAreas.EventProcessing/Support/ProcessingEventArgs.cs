﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;

namespace ControlAreas.EventProcessing.Support
{
    public class ProcessingEventArgs: EventArgs
    {
        private readonly EventData _dataCurrent;
        private readonly EventData _dataPrevious;

        public ProcessingEventArgs(EventData data)
        {
            _dataCurrent = data;
        }

        public ProcessingEventArgs(EventData dataCur, EventData dataPrev)
        {
            _dataCurrent = dataCur;
            _dataPrevious = dataPrev;
        }


        public EventData DataCurrent
        {
            get { return _dataCurrent; }
        }

        public EventData DataPrevious
        {
            get { return _dataPrevious; }
        }
    }
}
