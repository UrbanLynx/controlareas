﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.EventProcessing.Support;

namespace ControlAreas.EventProcessing.EventWorkers
{
    public class FirstAppearanceWork
    {
        protected EventManager Manager;

        public FirstAppearanceWork(EventManager eventManager)
        {
            Manager = eventManager;
        }

        public event EventHandler<ProcessingEventArgs> EventOk;
        //public event EventHandler<ProcessingEventArgs> NewEvent;

        public void ProcessFirstAppearance(EventData eventData)
        {
            if (IsCaCreatedLongAgo(eventData.ToArea))
            {
                if (!IsCameraOnBorder(eventData.Event.CameraId))
                {
                    var plate = Manager.RecognizePlate(eventData);
                    if (Manager.IsPlatesEqual(eventData.Event.Plate, plate))
                    {
                        ProcessRegistrationInfo(eventData);
                    }
                    else
                    {
                        Manager.FixPlates(eventData, plate);
                        Manager.ProcessEvent(eventData.Event);
                        return;
                    }
                }
            }
            if (eventData.Vehicle == null)
            {
                eventData.Vehicle = Manager.DbManager.MovementDb.AddVehicle((int) eventData.Event.PlateId,
                    (int) eventData.Event.VehicleBrandlId);
            }
            EventOk(this, new ProcessingEventArgs(eventData));
        }

        private void ProcessRegistrationInfo(EventData eventData)
        {
            eventData.VehicleOfficialInfo =
                    Manager.AdapterManager.VehicleInfoProvider.GetPlateInfo(eventData.Event.Plate);
            if (HasOfficialInfo(eventData))
            {
                
                var registratedRecently = IsPlateRegistratedRecently(eventData);
                var registratedInAnotherRegion = IsPlateRegistratedInAnotherRegion(eventData);
                if (!registratedRecently || registratedInAnotherRegion)
                {
                    eventData.Vehicle = Manager.DbManager.MovementDb.AddVehicle((int)eventData.Event.PlateId,
                        (int)eventData.Event.VehicleBrandlId);
                    Manager.DeclareWanted(eventData, "Первое появление. ГРЗ зарегестрирован в другом регионе");
                }
            }
        }

        private bool HasOfficialInfo(EventData eventData)
        {
            return eventData.VehicleOfficialInfo.OfficialPlate != null;
        }

        private bool IsPlateRegistratedInAnotherRegion(EventData eventData)
        {
            return eventData.Event.Camera.RegionCodeId ==
                   eventData.VehicleOfficialInfo.OfficialPlate.RegistrationRegionId;
        }

        private bool IsPlateRegistratedRecently(EventData eventData)
        {
            // Не круто захардкожено. Было зарегано в последние 5 мин
            return eventData.VehicleOfficialInfo.OfficialPlate.RegistrationTime > DateTime.Now.AddMinutes(-5);
        }

        private bool IsCameraOnBorder(int cameraId)
        {
            return Manager.DbManager.ControlAreaDb.IsCameraOnBorder(cameraId);
        }

        private bool IsCaCreatedLongAgo(ControlArea area)
        {
            // Не круто захардкожено
            return area.CreationTime < DateTime.Now.AddMinutes(-5);
        }
    }
}
