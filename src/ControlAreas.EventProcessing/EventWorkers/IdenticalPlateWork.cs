﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.EventProcessing.Support;

namespace ControlAreas.EventProcessing.EventWorkers
{
    public class IdenticalPlateWork
    {
        protected EventManager Manager;

        public IdenticalPlateWork(EventManager eventManager)
        {
            Manager = eventManager;
        }

        public event EventHandler<ProcessingEventArgs> EventOk;
        //public event EventHandler<ProcessingEventArgs> NewEvent;

        public void ProcessIdenticalPlates(EventData curEventData, EventData prevEventData)
        {
            // распознать марки, где их хранить? Может их просто всегда распознавать в истории, или они уже и так распознаются
            if (!Manager.IsBrandsEqual((int)curEventData.Event.VehicleBrandlId, (int)prevEventData.Event.VehicleBrandlId))
            {
                GetVehicleInfo(curEventData);
                if (!IsOfficialInfoEqualsFact(curEventData))
                {
                    curEventData.Vehicle = Manager.DbManager.MovementDb.AddVehicle((int)curEventData.Event.PlateId,
                        (int)curEventData.Event.VehicleBrandlId);
                    Manager.DeclareWanted(curEventData, "ТС 'Двойник'. Регистрационные данные не совпадают.");
                    curEventData.NewHistory();
                    EventOk(this, new ProcessingEventArgs(curEventData));
                    return;
                }
                GetVehicleInfo(prevEventData);
                if (!IsOfficialInfoEqualsFact(prevEventData))
                {
                    prevEventData.Vehicle = Manager.DbManager.MovementDb.AddVehicle((int)prevEventData.Event.PlateId,
                        (int)prevEventData.Event.VehicleBrandlId);
                    Manager.DbManager.MovementDb.Delete(prevEventData);
                    Manager.DeclareWanted(prevEventData, "ТС 'Двойник'. Регистрационные данные не совпадают.");
                    
                    prevEventData.NewHistory();
                    // таски для параллельной вставки
                    EventOk(this, new ProcessingEventArgs(prevEventData));
                    EventOk(this, new ProcessingEventArgs(curEventData));
                }
            }
            else
            {
                EventOk(this, new ProcessingEventArgs(curEventData));
            }
            
        }
        

        private void GetVehicleInfo(EventData curEventData)
        {
            curEventData.VehicleOfficialInfo =
                Manager.AdapterManager.VehicleInfoProvider.GetPlateInfo(curEventData.Vehicle.Plate);

        }

        private bool IsOfficialInfoEqualsFact(EventData eventData)
        {
            if (eventData.VehicleOfficialInfo.OfficialPlate == null)
            {
                return true;
            }
            return
                Manager.IsPlatesEqual((int) eventData.Event.PlateId, eventData.VehicleOfficialInfo.OfficialPlate.PlateId) &&
                Manager.IsBrandsEqual((int) eventData.Event.VehicleBrandlId,
                    eventData.VehicleOfficialInfo.OfficialPlate.VehicleBrandId);
        }
    }
}
