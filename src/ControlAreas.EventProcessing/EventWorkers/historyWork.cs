﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.EventProcessing.Support;
using ControlAreas.Research.Public;

namespace ControlAreas.EventProcessing.EventWorkers
{
    class HistoryWork
    {
        protected EventManager Manager;

        public event EventHandler<ProcessingEventArgs> NotInHistory;
        public event EventHandler<ProcessingEventArgs> RightRout;
        public event EventHandler<ProcessingEventArgs> WrongRout;

        public HistoryWork(EventManager manager)
        {
            Manager = manager;
        }

        public void ProcessMovementHistory(EventData curEventData)
        {
            var timer = new Stopwatch();
            timer.Start();
            var vehicleFound = FindVehicle(curEventData);

            timer.Stop();
            //Console.WriteLine("vehicleFound - {0}", timer.ElapsedMilliseconds);
            timer.Restart();
            if (vehicleFound)
            {
                ProcessOne(curEventData);
            }
            else
            {
                ResearchStatistics.AddFirstAppearance();
                NotInHistory(this, new ProcessingEventArgs(curEventData));
            }
            timer.Stop();
            //Console.WriteLine("event processed - {0}", timer.ElapsedMilliseconds);
        }

        private void ProcessOne(EventData curEventData)
        {
            var timer = new Stopwatch();
            timer.Start();
            var prevEventData = Manager.DbManager.MovementDb.GetPreviousEvent(curEventData);
            timer.Stop();
            //Console.WriteLine("ProcessOne - GetPreviousEvent - {0}", timer.ElapsedMilliseconds);
            timer.Restart();

            var check = CheckCameraConnection(curEventData, prevEventData);
            timer.Stop();
            //Console.WriteLine("ProcessOne - CheckCameraConnection - {0}", timer.ElapsedMilliseconds);
            
            if (check)
            {
                timer.Restart();

                ResearchStatistics.AddStandart();
                RightRout(this, new ProcessingEventArgs(curEventData));

                timer.Stop();
                //Console.WriteLine("ProcessOne - RightRout - {0}", timer.ElapsedMilliseconds);
            }
            else
            {
                timer.Restart();

                ResearchStatistics.AddNotStandart();
                WrongRout(this, new ProcessingEventArgs(curEventData, prevEventData));

                timer.Stop();
                //Console.WriteLine("ProcessOne - WrongRout - {0}", timer.ElapsedMilliseconds);
            }
            timer.Stop();
            //Console.WriteLine("ProcessOne - other - {0}", timer.ElapsedMilliseconds);
        }

        private bool CheckCameraConnection(EventData curEventData, EventData prevEventData)
        {
            return Manager.DbManager.ControlAreaDb.IsCamerasConnected(prevEventData.Event.CameraId,
                curEventData.Event.CameraId);
        }

        private bool FindVehicle(EventData eventData)
        {
            // получает все ТС для ГРЗ из события
            Debug.Assert(eventData.Event.PlateId != null, "eventData.Event.PlateId != null");
            var vehicles = Manager.DbManager.MovementDb.GetVehiclesFor((int) eventData.Event.PlateId);
            if (vehicles.Count > 1)
            {
                var brand = Manager.RecognizeBrand(eventData);
                // добавить эту марку в событие
                vehicles = Manager.DbManager.MovementDb.GetVehiclesFor((int)eventData.Event.PlateId, brand.Id);
            }
            if (vehicles.Count == 1)
            {
                eventData.Vehicle = vehicles.First();
                return true;
            }
            return false; // если Count == 0
            // вроде ситуации, когда есть несколько ТС с одним ГРЗ и маркой нет, так что к этому времени
        }
    }
}
