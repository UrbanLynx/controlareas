﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.EventProcessing.Support;

namespace ControlAreas.EventProcessing.EventWorkers
{
    public class TwinsWork
    {
        protected EventManager Manager;

        public TwinsWork(EventManager eventManager)
        {
            Manager = eventManager;
        }

        public event EventHandler<ProcessingEventArgs> IdenticalPlates;
        //public event EventHandler<ProcessingEventArgs> NewEvent;

        public void ProcessTwins(EventData curEventData, EventData prevEventData)
        {
            var curPlate = Manager.RecognizePlate(curEventData);
            var prevPlate = Manager.RecognizePlate(prevEventData);
            var curEventError = !Manager.IsPlatesEqual(curEventData.Vehicle.PlateId, curPlate.Id);
            var prevEventError = !Manager.IsPlatesEqual(prevEventData.Vehicle.PlateId, prevPlate.Id);
            if (curEventError)
            {
                Manager.FixPlates(curEventData,curPlate);
            }
            if (prevEventError)
            {
                Manager.DbManager.MovementDb.Delete(prevEventData);
                Manager.FixPlates(prevEventData,prevPlate);
            }
            if (curEventError)
            {
                // создать таск, чтобы выполнять отдельно обработку в разных потоках
                Manager.ProcessEvent(curEventData.Event);
            }
            if (prevEventError)
            {
                Manager.ProcessEvent(prevEventData.Event);
            }
            if (!curEventError && !prevEventError)
            {
                IdenticalPlates(this, new ProcessingEventArgs(curEventData, prevEventData));
            }
        }
    }
}
