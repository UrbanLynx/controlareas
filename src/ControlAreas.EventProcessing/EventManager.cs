﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Adapter;
using ControlAreas.Dbs;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.EventProcessing.EventWorkers;
using ControlAreas.EventProcessing.Support;
using ControlAreas.Research.Public;

namespace ControlAreas.EventProcessing
{
    public class EventManager
    {       

        private HistoryWork _historyWork;
        private FirstAppearanceWork _firstAppearance;
        private TwinsWork _twinsWork;
        private IdenticalPlateWork _identicalPlateWork;

        public EventManager(ViFlowContext db)
        {
            AdapterManager = new AdapterManager(db);
            DbManager = new DbManager(db);

            _historyWork = new HistoryWork(this);
            _firstAppearance = new FirstAppearanceWork(this);
            _twinsWork = new TwinsWork(this);
            _identicalPlateWork = new IdenticalPlateWork(this);

            SubscribeProcessors();
        }

        public AdapterManager AdapterManager { get; protected set; }
        public DbManager DbManager { get; protected set; }

        public void SubscribeProcessors()
        {
            _historyWork.NotInHistory += (sender, args) => _firstAppearance.ProcessFirstAppearance(args.DataCurrent);
            _historyWork.RightRout += (sender, args) => AddToAreaMovement(args.DataCurrent);
            _historyWork.WrongRout += (sender, args) => _twinsWork.ProcessTwins(args.DataCurrent, args.DataPrevious);

            _firstAppearance.EventOk += (sender, args) => AddToAreaMovement(args.DataCurrent);

            _twinsWork.IdenticalPlates +=
                (sender, args) => _identicalPlateWork.ProcessIdenticalPlates(args.DataCurrent, args.DataPrevious);
            // TODO: мб добавить события норм точек в _twinsWork

            _identicalPlateWork.EventOk += (sender, args) => AddToAreaMovement(args.DataCurrent);

        }

        public void ProcessEvent(Event curEvent)
        {
            if (EventValid(curEvent))
            {
                var eventData = new EventData(curEvent);
                DbManager.ControlAreaDb.AddCAto(eventData);
                _historyWork.ProcessMovementHistory(eventData);
            }
        }

        private bool EventValid(Event curEvent)
        {
            curEvent.Plate = DbManager.EventDb.GetPlateById((int)curEvent.PlateId);
            curEvent.Camera = DbManager.EventDb.GetCameraById(curEvent.CameraId);
            /*if (curEvent.Plate == null)
            {
                curEvent.Plate = DbManager.EventDb.GetPlateById((int) curEvent.PlateId);
            }
            if (curEvent.Camera == null)
            {
                curEvent.Camera = DbManager.EventDb.GetCameraById(curEvent.CameraId);
            }*/
            return PlateValid(curEvent.Plate) && DbManager.ControlAreaDb.CameraInCA(curEvent.Camera);
            // проверить, что есть plateId, vehicleBrandId и там все буквы распознаны. мб еще что то
            // проверить, что для камеры фиксации есть КО, т.е. наличитсвует CameraArea. Иначе не обрабатываем. 
            throw new NotImplementedException();
        }

        private bool PlateValid(Plate plate)
        {
            return plate.Recognized;
        }

        public VehicleBrand RecognizeBrand(EventData eventData)
        {
            return AdapterManager.RecognitionService.RecognizeBrand(eventData);
        }

        public Plate RecognizePlate(EventData eventData)
        {
            return AdapterManager.RecognitionService.RecognizePlate(eventData);
        }

        public bool IsPlatesEqual(Plate d, Plate p)
        {
            return d.N1 == p.N1 && d.N2 == p.N2 && d.N3 == p.N3 && d.N4 == p.N4 && d.N5 == p.N5 && d.N6 == p.N6 &&
                   d.N7 == p.N7 && d.N8 == p.N8 && d.N9 == p.N9 && d.N10 == p.N10;
        }

        public void DeclareWanted(EventData eventData, string reason)
        {
            eventData.WantedReason = reason;
            DbManager.MovementDb.DeclareWanted(eventData);
        }

        public void FixPlates(EventData eventData, Plate plate)
        {
            eventData.Event.PlateId = plate.Id;
        }

        public bool IsBrandsEqual(VehicleBrand brand1, VehicleBrand brand2)
        {
            return brand1.Name == brand2.Name;
        }

        public void AddToAreaMovement(EventData eventData)
        {
            var timer = new Stopwatch();
            timer.Start();

            DbManager.MovementDb.Add(eventData);

            timer.Stop();
            //Console.WriteLine("AddToAreaMovement - {0}", timer.ElapsedMilliseconds);
            
        }

        public bool IsPlatesEqual(int plateAid, int plateBid)
        {
            var plateA = DbManager.EventDb.GetPlateById(plateAid);
            var plateB = DbManager.EventDb.GetPlateById(plateBid);
            return IsPlatesEqual(plateA, plateB);
        }

        public bool IsBrandsEqual(int brandAid, int brandBid)
        {
            var brandA = DbManager.EventDb.GetBrandById(brandAid);
            var brandB = DbManager.EventDb.GetBrandById(brandBid);
            return IsBrandsEqual(brandA, brandB);
        }
    }
}
