﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAreas.Dbs.Entities.DbEntities
{
    [Table("RealVehicle")]
    public partial class RealVehicle
    {
        public int Id { get; set; }
        public int PlateId { get; set; }
        public int VehicleBrandId { get; set; }
        public int? OfficialPlatedId { get; set; }

        [ForeignKey("PlateId")]
        public virtual Plate Plate { get; set; }
        [ForeignKey("VehicleBrandId")]
        public virtual VehicleBrand VehicleBrand { get; set; }
        [ForeignKey("OfficialPlatedId")]
        public virtual OfficialPlate OfficialPlate { get; set; }

        public virtual ICollection<RealEvent> RealEvent { get; set; }
    }
}
