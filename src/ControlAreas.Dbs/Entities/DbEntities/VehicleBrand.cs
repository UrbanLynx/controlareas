namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleBrand")]
    public partial class VehicleBrand
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
