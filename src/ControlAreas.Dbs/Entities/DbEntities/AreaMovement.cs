namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AreaMovement")]
    public partial class AreaMovement
    {
        public long Id { get; set; }

        public int VehicleId { get; set; }

        public int ControlAreaId { get; set; }

        public long? FromEventId { get; set; }

        public long? ToEventId { get; set; }

        [ForeignKey("ControlAreaId")]
        public virtual ControlArea ControlArea { get; set; }
        [ForeignKey("FromEventId")]
        public virtual Event FromEvent { get; set; }
        [ForeignKey("ToEventId")]
        public virtual Event ToEvent { get; set; }
        [ForeignKey("VehicleId")]
        public virtual Vehicle Vehicle { get; set; }
    }
}
