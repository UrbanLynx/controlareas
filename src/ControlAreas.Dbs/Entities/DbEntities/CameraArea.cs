namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CameraArea")]
    public partial class CameraArea
    {
        public long Id { get; set; }

        public int CameraId { get; set; }

        public int FromAreaId { get; set; }

        public int ToAreaId { get; set; }
        [ForeignKey("CameraId")]
        public virtual Camera Camera { get; set; }
        [ForeignKey("FromAreaId")]
        public virtual ControlArea FromArea { get; set; }
        [ForeignKey("ToAreaId")]
        public virtual ControlArea ToArea { get; set; }
    }
}
