namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ControlArea")]
    public partial class ControlArea
    {
        public int Id { get; set; }

        public DateTime CreationTime { get; set; }

        public bool IsExternal { get; set; }

        public virtual ICollection<AreaMovement> AreaMovement { get; set; }

        public virtual ICollection<CameraArea> CameraArea { get; set; }

        public virtual ICollection<CameraArea> CameraArea1 { get; set; }
    }
}
