namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Camera")]
    public partial class Camera
    {
        public int Id { get; set; }

        public int RegionCodeId { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
        [ForeignKey("RegionCodeId")]
        public virtual Region RegionCode { get; set; }

        public virtual ICollection<CameraArea> CameraArea { get; set; }

        public virtual ICollection<Event> Event { get; set; }
    }
}
