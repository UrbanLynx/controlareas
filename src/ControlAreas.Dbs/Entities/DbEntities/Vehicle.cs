namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vehicle")]
    public partial class Vehicle
    {
        public int Id { get; set; }

        public int PlateId { get; set; }

        public int VehicleBrandId { get; set; }



        [ForeignKey("PlateId")]
        public virtual Plate Plate { get; set; }
        [ForeignKey("VehicleBrandId")]
        public virtual VehicleBrand VehicleBrand { get; set; }

        public virtual ICollection<WantedVehicle> WantedVehicle { get; set; }
        public virtual ICollection<AreaMovement> AreaMovement { get; set; }
    }
}
