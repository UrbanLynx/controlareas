namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Region")]
    public partial class Region
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Code { get; set; }

        public virtual ICollection<Plate> Plate { get; set; }

        public virtual ICollection<Camera> Camera { get; set; }
        public virtual ICollection<OfficialPlate> OfficialPlate { get; set; } 
    }
}
