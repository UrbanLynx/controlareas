﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAreas.Dbs.Entities.DbEntities
{
    [Table("OfficialPlate")]
    public partial class OfficialPlate
    {
        public int Id { get; set; }

        public int VehicleBrandId { get; set; }

        public int PlateId { get; set; }

        public int RegistrationRegionId { get; set; }

        public DateTime RegistrationTime { get; set; }

        [ForeignKey("PlateId")]
        public virtual Plate Plate { get; set; }
        [ForeignKey("VehicleBrandId")]
        public virtual VehicleBrand VehicleBrand { get; set; }
        [ForeignKey("RegistrationRegionId")]
        public virtual Region RegistrationRegion { get; set; }
    }
}
