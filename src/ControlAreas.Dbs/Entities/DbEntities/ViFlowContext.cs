﻿using System.Data.Entity.Infrastructure.Annotations;

namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ViFlowContext : DbContext
    {
        public ViFlowContext()
            : base("name=ViFlowContext")
        {
        }

        /*public ViFlowContext()
        {
        }*/

        public virtual DbSet<AreaMovement> AreaMovement { get; set; }
        public virtual DbSet<Camera> Camera { get; set; }
        public virtual DbSet<CameraArea> CameraArea { get; set; }
        public virtual DbSet<ControlArea> ControlArea { get; set; }
        public virtual DbSet<OfficialPlate> OfficialPlate { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Plate> Plate { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RealEvent> RealEvent { get; set; }
        public virtual DbSet<RealVehicle> RealVehicle { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleBrand> VehicleBrand { get; set; }
        public virtual DbSet<WantedVehicle> WantedVehicle { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Camera>()
                .HasMany(e => e.CameraArea)
                .WithRequired(e => e.Camera)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Camera>()
                .HasMany(e => e.Event)
                .WithRequired(e => e.Camera)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ControlArea>()
                .HasMany(e => e.AreaMovement)
                .WithRequired(e => e.ControlArea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ControlArea>()
                .HasMany(e => e.CameraArea)
                .WithRequired(e => e.FromArea)
                .HasForeignKey(e => e.FromAreaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ControlArea>()
                .HasMany(e => e.CameraArea1)
                .WithRequired(e => e.ToArea)
                .HasForeignKey(e => e.ToAreaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.ToAreaMovement)
                .WithOptional(e => e.FromEvent)
                .HasForeignKey(e => e.FromEventId);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.FromAreaMovement)
                .WithOptional(e => e.ToEvent)
                .HasForeignKey(e => e.ToEventId);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.RealEvent)
                .WithRequired(e => e.Event)
                .HasForeignKey(e => e.EventId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N1)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N2)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N3)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N4)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N5)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N6)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N7)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N8)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N9)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .Property(e => e.N10)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Plate>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.Plate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.AreaMovement)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.WantedVehicle)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleBrand>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.OfficialPlate)
                .WithRequired(e => e.RegistrationRegion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RealVehicle>()
                .HasMany(e => e.RealEvent)
                .WithRequired(e => e.RealVehicle)
                .HasForeignKey(e => e.RealVehicleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RealEvent>()
                .HasIndex("Index_RealVehicleId_Id",
                e => e.Property(x => x.RealVehicleId),
                e => e.Property(x => x.EventId),
                e => e.Property(x => x.Id));

            modelBuilder.Entity<Event>()
                .HasIndex("Index_Time_Id",
                e => e.Property(x => x.Time),
                e => e.Property(x => x.Id));

            modelBuilder.Entity<AreaMovement>()
                .HasIndex("Index_VehicleId_Id",
                e => e.Property(x => x.VehicleId),
                e => e.Property(x => x.Id));

        }
    }
}
