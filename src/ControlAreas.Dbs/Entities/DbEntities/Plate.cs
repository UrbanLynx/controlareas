using System.Linq;

namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Plate")]
    public partial class Plate
    {
        public Plate() { }
        public Plate(string plateStr)
        {
            var plateLength = 10;
            if (plateStr.Count() == plateLength)
            {
                N1 = NfromChar(plateStr[0]);
                N2 = NfromChar(plateStr[1]);
                N3 = NfromChar(plateStr[2]);
                N4 = NfromChar(plateStr[3]);
                N5 = NfromChar(plateStr[4]);
                N6 = NfromChar(plateStr[5]);
                N7 = NfromChar(plateStr[6]);
                N8 = NfromChar(plateStr[7]);
                N9 = NfromChar(plateStr[8]);
                N10 = NfromChar(plateStr[9]);
            }
        }

        public string NfromChar(char c)
        {
            var anyChar = '-';
            if (c != anyChar)
            {
                return c.ToString();
            }
            return null;
        }

        public int Id { get; set; }

        public int RegionCodeId { get; set; }

        public bool Recognized { get; set; }

        [StringLength(1)]
        public string N1 { get; set; }

        [StringLength(1)]
        public string N2 { get; set; }

        [StringLength(1)]
        public string N3 { get; set; }

        [StringLength(1)]
        public string N4 { get; set; }

        [StringLength(1)]
        public string N5 { get; set; }

        [StringLength(1)]
        public string N6 { get; set; }

        [StringLength(1)]
        public string N7 { get; set; }

        [StringLength(1)]
        public string N8 { get; set; }

        [StringLength(1)]
        public string N9 { get; set; }

        [StringLength(1)]
        public string N10 { get; set; }


        public virtual ICollection<Event> Event { get; set; }
        [ForeignKey("RegionCodeId")]
        public virtual Region RegionCode { get; set; }

        public virtual ICollection<Vehicle> Vehicle { get; set; }

        public virtual ICollection<OfficialPlate> OfficialPlate { get; set; } 
    }
}
