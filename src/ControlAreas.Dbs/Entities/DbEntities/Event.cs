namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Event")]
    public partial class Event
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public int? PlateId { get; set; }

        public int CameraId { get; set; }

        public int? VehicleBrandlId { get; set; }

        public DateTime Time { get; set; }

        public long PhotoId { get; set; }

        // ������ ����� ������ ICollection, �.�. ����� 1 � 1, � �� 1 �� ������
        public virtual ICollection<AreaMovement> ToAreaMovement { get; set; }
        public virtual ICollection<AreaMovement> FromAreaMovement { get; set; }
        public virtual ICollection<RealEvent> RealEvent { get; set; }

        [ForeignKey("CameraId")]
        public virtual Camera Camera { get; set; }
        [ForeignKey("PlateId")]
        public virtual Plate Plate { get; set; }
        [ForeignKey("VehicleBrandlId")]
        public virtual VehicleBrand VehicleBrand { get; set; }
    }
}
