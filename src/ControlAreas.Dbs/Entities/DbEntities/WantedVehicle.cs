namespace ControlAreas.Dbs.Entities.DbEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WantedVehicle")]
    public partial class WantedVehicle
    {
        public int Id { get; set; }

        public int VehicleId { get; set; }

        public string Reason { get; set; }
        [ForeignKey("VehicleId")]
        public virtual Vehicle Vehicle { get; set; }
    }
}
