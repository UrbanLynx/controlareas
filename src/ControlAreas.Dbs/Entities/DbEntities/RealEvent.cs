﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAreas.Dbs.Entities.DbEntities
{
    [Table("RealEvent")]
    public partial class RealEvent
    {
        public long Id { get; set; }
        public int RealVehicleId { get; set; }
        public long EventId { get; set; }

        [ForeignKey("RealVehicleId")]
        public RealVehicle RealVehicle { get; set; }
        [ForeignKey("EventId")]
        public Event Event { get; set; }
    }
}
