﻿using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.Entities
{
    public class VehicleOfficialInfo
    {
        public OfficialPlate OfficialPlate { get; set; }
    }
}
