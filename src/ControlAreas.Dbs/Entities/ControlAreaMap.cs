﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.Entities
{
    public class ControlAreaMap
    {
        public ControlArea ControlArea { get; set; }
        public List<Camera> Cameras { get; set; }
    }
}
