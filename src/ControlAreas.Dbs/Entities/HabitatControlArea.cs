﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.Entities
{
    public class HabitatControlArea
    {
        public ControlArea ControlArea { get; set; }
        public int VisitNumber { get; set; }
    }
}
