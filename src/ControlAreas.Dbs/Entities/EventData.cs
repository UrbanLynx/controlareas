﻿using System;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.Entities
{
    public class EventData
    {
        public EventData(Event curEvent)
        {
            Event = curEvent;
        }

        public Event Event { get; set; }
        public ControlArea FromArea { get; set; }
        public ControlArea ToArea { get; set; }
        public VehicleOfficialInfo VehicleOfficialInfo { get; set; }
        public Vehicle Vehicle { get; set; }
        public string WantedReason { get; set; }

        public void NewHistory()
        {
            
        }
    }
}
