﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.DbWorkers;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs
{
    public class DbManager
    {
        public DbManager(ViFlowContext db)
        {
            MovementDb = new MovementDbWork(db);
            ControlAreaDb = new ControlAreaDbWork(db);
            EventDb = new EventDbWork(db);
        }
        public MovementDbWork MovementDb { get; protected set; }
        public ControlAreaDbWork ControlAreaDb { get; protected set; }
        public EventDbWork EventDb { get; protected set; }

        public void Initialize() { }
    }
}
