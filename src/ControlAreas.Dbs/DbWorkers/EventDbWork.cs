﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;
using System.Data.Entity;

namespace ControlAreas.Dbs.DbWorkers
{
    public class EventDbWork
    {
        private ViFlowContext db;

        public EventDbWork(ViFlowContext viFlowContext)
        {
            db = viFlowContext;
        }

        public void Initialize()
        {

        }

        public List<Event> GetEvents(DateTime from, DateTime to)
        {
            throw new NotImplementedException();
            // нужно включать plate в ответ или еще как то, а то потом ексепшн
            return db.Event.Where(x => x.Time > from && x.Time <= to).ToList();
        }

        public List<Event> GetOldEvents(int startId)
        {
            return db.Event.Include(e => e.Plate).Include(e => e.VehicleBrand).Where(e => e.Id > startId).ToList();
        }

        public Plate GetPlateById(int plateId)
        {
            return db.Plate.SingleOrDefault(x => x.Id == plateId);
        }

        public VehicleBrand GetBrandById(int brandId)
        {
            return db.VehicleBrand.SingleOrDefault(x => x.Id == brandId);
        }

        public Camera GetCameraById(int cameraId)
        {
            return db.Camera.SingleOrDefault(x => x.Id == cameraId);
        }
    }
}
