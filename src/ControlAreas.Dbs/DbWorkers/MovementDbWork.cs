﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.DbWorkers
{
    public class MovementDbWork
    {
        private ViFlowContext db;

        public MovementDbWork(ViFlowContext viFlowContext)
        {
            db = viFlowContext;
        }

        public void Initialize()
        {
            
        }

        public List<Vehicle> GetVehiclesFor(int plateId)
        {
            return db.Vehicle.Where(v => v.PlateId == plateId).ToList();
        }

        /*public Vehicle GetVehicleFor(int plateId, int brandId)
        {
            return db.Vehicle.SingleOrDefault(v => v.PlateId == plateId && v.VehicleBrandId == brandId);
        }*/

        public List<Vehicle> GetVehiclesFor(int plateId, int brandId)
        {
            return db.Vehicle.Where(v => v.PlateId == plateId && v.VehicleBrandId == brandId).ToList();
        }

        public EventData GetPreviousEvent(EventData curEvent)
        {
            using (var context = new ViFlowContext())
            {
                var fromEvent =
                context.AreaMovement.Where(a => a.VehicleId == curEvent.Vehicle.Id && a.FromEvent.Time < curEvent.Event.Time)
                    .Select(a => a.FromEvent)
                    .OrderByDescending(a => a.Time)
                    .FirstOrDefault();
                if (fromEvent != null)
                {
                    var prevEvent = new EventData(fromEvent);
                    var fromAreaId = prevEvent.Event.FromAreaMovement.FirstOrDefault().ControlAreaId;
                    prevEvent.FromArea = context.ControlArea.SingleOrDefault(c => c.Id == fromAreaId);

                    var toAreaId = prevEvent.Event.ToAreaMovement.FirstOrDefault().ControlAreaId;
                    prevEvent.ToArea = context.ControlArea.SingleOrDefault(c => c.Id == toAreaId);
                    prevEvent.Vehicle = curEvent.Vehicle;
                    return prevEvent;
                }
                return null;
            }
            
                 /* .OrderBy(a => a.FromEvent.Time)
                    .FirstOrDefault();*/
            throw new NotImplementedException();
        }

        public EventData GetNextEvent(EventData curEvent)
        {
            throw new NotImplementedException();
        }

        public Vehicle AddVehicle(int plateId, int brandId)
        {
            var vehicle = new Vehicle()
            {
                PlateId = plateId,
                VehicleBrandId = brandId
            };
            db.Vehicle.Add(vehicle);
            db.SaveChanges();
            return vehicle;
        }

        public AreaMovement GetPreviousMovement(EventData curEvent, ViFlowContext context)
        {
            var movement = context.AreaMovement.Where(m => m.VehicleId == curEvent.Vehicle.Id && m.FromEvent.Time < curEvent.Event.Time)
                    .OrderByDescending(m => m.FromEvent.Time)
                    .FirstOrDefault();
            if (movement != null && movement.ControlAreaId == curEvent.FromArea.Id)
            {
                return movement;
            }
            return null;
        }

        public AreaMovement GetCurrentMovement(EventData curEvent, ViFlowContext context)
        {
            var movement = context.AreaMovement.Where(m => m.VehicleId == curEvent.Vehicle.Id && m.ToEvent.Time > curEvent.Event.Time)
                    .OrderBy(m => m.ToEvent.Time)
                    .FirstOrDefault();
            if (movement != null && movement.ControlAreaId == curEvent.ToArea.Id)
            {
                return movement;
            }
            return null;
        }

        public AreaMovement GetNextMovement(EventData curEvent)
        {
            throw new NotImplementedException();
        }

        public void Delete(EventData delEvent)
        {
            //var prevMovement = GetPreviousMovement(delEvent);
            var prevMovement = db.AreaMovement.SingleOrDefault(m => m.ToEventId == delEvent.Event.Id);
            prevMovement.ToEventId = null;
            if (prevMovement.FromEventId == null)
            {
                db.AreaMovement.Remove(prevMovement);
            }

            //var curMovement = GetCurrentMovement(delEvent);
            var curMovement = db.AreaMovement.SingleOrDefault(m => m.FromEventId == delEvent.Event.Id);
            curMovement.FromEventId = null;
            if (curMovement.ToEventId == null)
            {
                db.AreaMovement.Remove(curMovement);
            }

            db.SaveChanges();
        }

        public void Add(EventData addEvent)
        {
            using (var context = new ViFlowContext())
            {
                var prevMovement = GetPreviousMovement(addEvent, context);
                if (prevMovement == null)
                {
                    prevMovement = new AreaMovement()
                    {
                        ControlAreaId = addEvent.FromArea.Id,
                        VehicleId = addEvent.Vehicle.Id
                    };
                    context.AreaMovement.Add(prevMovement);
                }
                prevMovement.ToEventId = addEvent.Event.Id;
                context.SaveChanges();

                var curMovement = GetCurrentMovement(addEvent, context);
                if (curMovement == null)
                {
                    curMovement = new AreaMovement()
                    {
                        ControlAreaId = addEvent.ToArea.Id,
                        VehicleId = addEvent.Vehicle.Id
                    };
                    context.AreaMovement.Add(curMovement);
                }
                curMovement.FromEventId = addEvent.Event.Id;

                context.SaveChanges();
            }
        }


        public List<Vehicle> GetAllVehicles()
        {
            return db.Vehicle.ToList();
        }

        public List<AreaMovement> GetAreaMovementsFor(int vehicleId)
        {
            return db.AreaMovement.Where(a => a.VehicleId == vehicleId).ToList();
        }

        public List<HabitatControlArea> GetHabitat(int vehicleId)
        {
            var controlAreas1 = db.AreaMovement.Where(a => a.VehicleId == vehicleId).GroupBy(a => a.ControlAreaId);

            var controlAreas2 = controlAreas1.Select(group => new
            {
                Key = group.Key,
                Count = group.Count(),
                ControlArea = group.FirstOrDefault().ControlArea
            }).OrderByDescending(g => g.Count);
            
            var controlAreas3 = controlAreas2.Select(g => new HabitatControlArea(){ControlArea = g.ControlArea, VisitNumber = g.Count}).ToList();
            //return controlAreas.Select(c => db.ControlArea.Find(c)).ToList();
            return controlAreas3;
        }

        public HabitatControlArea GetHabitat(int vehicleId, int areaId)
        {
            var visits = db.AreaMovement.Where(m => m.ControlAreaId == areaId && m.VehicleId == vehicleId).Count();
            return new HabitatControlArea
            {
                ControlArea = db.ControlArea.SingleOrDefault(c => c.Id == areaId),
                VisitNumber = visits
            };
        }

        public void DeclareWanted(EventData eventData)
        {
            var wanted = new WantedVehicle()
            {
                VehicleId = eventData.Vehicle.Id,
                Reason = eventData.WantedReason
            };
            db.WantedVehicle.Add(wanted);
            db.SaveChanges();
        }

        public List<AreaMovement> GetMovementsOnTime(int vehicleId, DateTime time)
        {
            var resultControlAreas = new List<AreaMovement>();
            var fromArea = db.AreaMovement.Where(m => m.VehicleId == vehicleId && m.FromEventId != null && m.FromEvent.Time < time)
                .OrderByDescending(m => m.FromEvent.Time)
                .FirstOrDefault();
            if (fromArea != null)
            {
                resultControlAreas.Add(fromArea);
            }
            var toArea = db.AreaMovement.Where(m => m.VehicleId == vehicleId && m.ToEventId != null && m.ToEvent.Time > time)
                .OrderBy(m => m.ToEvent.Time)
                .FirstOrDefault();
            if (toArea != null && toArea != fromArea)
            {
                resultControlAreas.Add(toArea);
            }
            return resultControlAreas;
        }

        public List<AreaMovement> GetControlAreaMovements(int areaId, DateTime fromTime, DateTime toTime)
        {
            var fromArea =
                db.AreaMovement.Where(
                    m => m.ControlAreaId == areaId && m.FromEvent != null && m.FromEvent.Time <= fromTime && m.ToEvent == null).ToList();
            var toArea =
                db.AreaMovement.Where(
                    m => m.ControlAreaId == areaId && m.ToEvent != null && m.ToEvent.Time >= fromTime && m.FromEvent == null).ToList();
            var fromToArea =
                db.AreaMovement.Where(
                    m => m.ControlAreaId == areaId && m.FromEvent != null && m.ToEvent != null &&
                         m.FromEvent.Time <= fromTime && m.ToEvent.Time >= fromTime).ToList();

            return fromArea.Union(toArea).Union(fromToArea).ToList();
        }

        public List<WantedVehicle> GetAllWantedVehicles()
        {
            return db.WantedVehicle.ToList();
        }


        public List<Vehicle> GetVehicleByPlate(string plateStr)
        {
            var plate = new Plate(plateStr);
            var vehicles = db.Vehicle.AsEnumerable().Where(v => PlateMask(plate, v.Plate)).ToList();
            return vehicles;
        }

        private bool PlateMask(Plate plateMask, Plate plate)
        {
            return CharMask(plateMask.N1, plate.N1) &&
                   CharMask(plateMask.N2, plate.N2) &&
                   CharMask(plateMask.N3, plate.N3) &&
                   CharMask(plateMask.N4, plate.N4) &&
                   CharMask(plateMask.N5, plate.N5) &&
                   CharMask(plateMask.N6, plate.N6) &&
                   CharMask(plateMask.N7, plate.N7) &&
                   CharMask(plateMask.N8, plate.N8) &&
                   CharMask(plateMask.N9, plate.N9) &&
                   CharMask(plateMask.N10, plate.N10);
        }

        private bool CharMask(string charMask, string c)
        {
            if (charMask == null)
            {
                return true;
            }
            return charMask == c;
        }
    }
}
