﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Dbs.DbWorkers
{
    public class ControlAreaDbWork
    {
        private ViFlowContext db;

        public ControlAreaDbWork(ViFlowContext viFlowContext)
        {
            db = viFlowContext;
        }

        public void Initialize()
        {

        }

        public void AddCAto(EventData data)
        {
            var camearArea = db.CameraArea.SingleOrDefault(c => c.CameraId == data.Event.CameraId);
            if (camearArea != null)
            {
                data.FromArea = camearArea.FromArea;
                data.ToArea = camearArea.ToArea;
            }
        }

        public bool IsCamerasConnected(int cameraPrevId, int cameraCurId)
        {
            // with 'using context' comes exception because of lazy query
            var areaPrev = db.CameraArea.SingleOrDefault(c => c.CameraId == cameraPrevId);
            var areaCur = db.CameraArea.SingleOrDefault(c => c.CameraId == cameraCurId);

            if (areaPrev.ToAreaId == areaCur.FromAreaId)
            {
                return true;
            }
            return false;
            
        }

        public DateTime GetCreationTime(int controlAreaeId)
        {
            throw new NotImplementedException();
        }

        public bool IsCameraOnBorder(int cameraId)
        {
            // не учитывает, что камера может быть без зон. проверяется в Manager.EventValid
            return db.CameraArea.Single(ca => ca.CameraId == cameraId).FromArea.IsExternal;
            throw new NotImplementedException();
        }

        public ControlAreaMap GetCamerasFor(int controlAreaId)
        {
            var cameras = db.CameraArea.Where(c => c.ToAreaId == controlAreaId).Select(c => c.Camera).ToList();
            return new ControlAreaMap()
            {
                Cameras = cameras,
                ControlArea = db.ControlArea.SingleOrDefault(c => c.Id == controlAreaId)
            };
        }

        public bool CameraInCA(Camera camera)
        {
            return db.CameraArea.Any(c => c.Id == camera.Id);
        }

        public bool IsAreaExternal(int areaId)
        {
            return db.ControlArea.SingleOrDefault(c => c.Id == areaId).IsExternal;
        }

        public List<ControlAreaMap> GetAllControlAreaMap()
        {
            var controlAreas = db.ControlArea.ToList();
            var controlAreasMap = controlAreas.Select(c => GetCamerasFor(c.Id)).ToList();
            return controlAreasMap;
        }

        public ControlAreaMap GetLastControlAreaFor(int vehicleId)
        {
            var movement =
                db.AreaMovement.Where(m => m.VehicleId == vehicleId && m.FromEvent != null)
                    .OrderByDescending(m => m.FromEvent.Time)
                    .FirstOrDefault();
            return GetCamerasFor((int) movement.ControlAreaId);
        }
    }
}
