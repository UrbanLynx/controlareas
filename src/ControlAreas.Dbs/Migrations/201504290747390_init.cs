namespace ControlAreas.Dbs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AreaMovement",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        VehicleId = c.Int(nullable: false),
                        ControlAreaId = c.Int(nullable: false),
                        FromEventId = c.Long(),
                        ToEventId = c.Long(),
                        Event_Id = c.Long(),
                        Event_Id1 = c.Long(),
                        Event_Id2 = c.Long(),
                        Event1_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ControlArea", t => t.ControlAreaId, cascadeDelete: true)
                .ForeignKey("dbo.Event", t => t.Event_Id)
                .ForeignKey("dbo.Event", t => t.Event_Id1)
                .ForeignKey("dbo.Vehicle", t => t.VehicleId, cascadeDelete: true)
                .ForeignKey("dbo.Event", t => t.Event_Id2)
                .ForeignKey("dbo.Event", t => t.Event1_Id)
                .Index(t => t.VehicleId)
                .Index(t => t.ControlAreaId)
                .Index(t => t.Event_Id)
                .Index(t => t.Event_Id1)
                .Index(t => t.Event_Id2)
                .Index(t => t.Event1_Id);
            
            CreateTable(
                "dbo.ControlArea",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        IsExternal = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CameraArea",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CameraId = c.Int(nullable: false),
                        FromAreaId = c.Int(nullable: false),
                        ToAreaId = c.Int(nullable: false),
                        ControlArea_Id = c.Int(),
                        ControlArea1_Id = c.Int(),
                        ControlArea_Id1 = c.Int(),
                        ControlArea_Id2 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Camera", t => t.CameraId, cascadeDelete: true)
                .ForeignKey("dbo.ControlArea", t => t.ControlArea_Id)
                .ForeignKey("dbo.ControlArea", t => t.ControlArea1_Id)
                .ForeignKey("dbo.ControlArea", t => t.ControlArea_Id1)
                .ForeignKey("dbo.ControlArea", t => t.ControlArea_Id2)
                .Index(t => t.CameraId)
                .Index(t => t.ControlArea_Id)
                .Index(t => t.ControlArea1_Id)
                .Index(t => t.ControlArea_Id1)
                .Index(t => t.ControlArea_Id2);
            
            CreateTable(
                "dbo.Camera",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegionCodeId = c.Int(nullable: false),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                        Region_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.Region_Id)
                .Index(t => t.Region_Id);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        PlateId = c.Int(),
                        CameraId = c.Int(nullable: false),
                        VehicleBrandlId = c.Int(),
                        Time = c.DateTime(nullable: false),
                        PhotoId = c.Long(nullable: false),
                        VehicleBrand_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Camera", t => t.CameraId, cascadeDelete: true)
                .ForeignKey("dbo.Plate", t => t.PlateId)
                .ForeignKey("dbo.VehicleBrand", t => t.VehicleBrand_Id)
                .Index(t => t.PlateId)
                .Index(t => t.CameraId)
                .Index(t => t.VehicleBrand_Id);
            
            CreateTable(
                "dbo.Plate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegionCodeId = c.Int(nullable: false),
                        Recognized = c.Boolean(nullable: false),
                        N1 = c.String(maxLength: 1),
                        N2 = c.String(maxLength: 1),
                        N3 = c.String(maxLength: 1),
                        N4 = c.String(maxLength: 1),
                        N5 = c.String(maxLength: 1),
                        N6 = c.String(maxLength: 1),
                        N7 = c.String(maxLength: 1),
                        N8 = c.String(maxLength: 1),
                        N9 = c.String(maxLength: 1),
                        N10 = c.String(maxLength: 1),
                        Region_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.Region_Id)
                .Index(t => t.Region_Id);
            
            CreateTable(
                "dbo.OfficialPlate",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        VehicleBrandId = c.Int(nullable: false),
                        PlateId = c.Int(nullable: false),
                        RegistrationRegionId = c.Int(nullable: false),
                        RegistrationTime = c.DateTime(nullable: false),
                        Region_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plate", t => t.PlateId, cascadeDelete: true)
                .ForeignKey("dbo.Region", t => t.Region_Id)
                .ForeignKey("dbo.VehicleBrand", t => t.VehicleBrandId, cascadeDelete: true)
                .Index(t => t.VehicleBrandId)
                .Index(t => t.PlateId)
                .Index(t => t.Region_Id);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Code = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VehicleBrand",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlateId = c.Int(nullable: false),
                        VehicleBrandId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plate", t => t.PlateId, cascadeDelete: true)
                .ForeignKey("dbo.VehicleBrand", t => t.VehicleBrandId, cascadeDelete: true)
                .Index(t => t.PlateId)
                .Index(t => t.VehicleBrandId);
            
            CreateTable(
                "dbo.WantedVehicle",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        VehicleId = c.Int(nullable: false),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehicle", t => t.VehicleId, cascadeDelete: true)
                .Index(t => t.VehicleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AreaMovement", "Event1_Id", "dbo.Event");
            DropForeignKey("dbo.AreaMovement", "Event_Id2", "dbo.Event");
            DropForeignKey("dbo.CameraArea", "ControlArea_Id2", "dbo.ControlArea");
            DropForeignKey("dbo.CameraArea", "ControlArea_Id1", "dbo.ControlArea");
            DropForeignKey("dbo.CameraArea", "ControlArea1_Id", "dbo.ControlArea");
            DropForeignKey("dbo.CameraArea", "ControlArea_Id", "dbo.ControlArea");
            DropForeignKey("dbo.Event", "VehicleBrand_Id", "dbo.VehicleBrand");
            DropForeignKey("dbo.WantedVehicle", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "VehicleBrandId", "dbo.VehicleBrand");
            DropForeignKey("dbo.Vehicle", "PlateId", "dbo.Plate");
            DropForeignKey("dbo.AreaMovement", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.OfficialPlate", "VehicleBrandId", "dbo.VehicleBrand");
            DropForeignKey("dbo.OfficialPlate", "Region_Id", "dbo.Region");
            DropForeignKey("dbo.Plate", "Region_Id", "dbo.Region");
            DropForeignKey("dbo.Camera", "Region_Id", "dbo.Region");
            DropForeignKey("dbo.OfficialPlate", "PlateId", "dbo.Plate");
            DropForeignKey("dbo.Event", "PlateId", "dbo.Plate");
            DropForeignKey("dbo.Event", "CameraId", "dbo.Camera");
            DropForeignKey("dbo.AreaMovement", "Event_Id1", "dbo.Event");
            DropForeignKey("dbo.AreaMovement", "Event_Id", "dbo.Event");
            DropForeignKey("dbo.CameraArea", "CameraId", "dbo.Camera");
            DropForeignKey("dbo.AreaMovement", "ControlAreaId", "dbo.ControlArea");
            DropIndex("dbo.WantedVehicle", new[] { "VehicleId" });
            DropIndex("dbo.Vehicle", new[] { "VehicleBrandId" });
            DropIndex("dbo.Vehicle", new[] { "PlateId" });
            DropIndex("dbo.OfficialPlate", new[] { "Region_Id" });
            DropIndex("dbo.OfficialPlate", new[] { "PlateId" });
            DropIndex("dbo.OfficialPlate", new[] { "VehicleBrandId" });
            DropIndex("dbo.Plate", new[] { "Region_Id" });
            DropIndex("dbo.Event", new[] { "VehicleBrand_Id" });
            DropIndex("dbo.Event", new[] { "CameraId" });
            DropIndex("dbo.Event", new[] { "PlateId" });
            DropIndex("dbo.Camera", new[] { "Region_Id" });
            DropIndex("dbo.CameraArea", new[] { "ControlArea_Id2" });
            DropIndex("dbo.CameraArea", new[] { "ControlArea_Id1" });
            DropIndex("dbo.CameraArea", new[] { "ControlArea1_Id" });
            DropIndex("dbo.CameraArea", new[] { "ControlArea_Id" });
            DropIndex("dbo.CameraArea", new[] { "CameraId" });
            DropIndex("dbo.AreaMovement", new[] { "Event1_Id" });
            DropIndex("dbo.AreaMovement", new[] { "Event_Id2" });
            DropIndex("dbo.AreaMovement", new[] { "Event_Id1" });
            DropIndex("dbo.AreaMovement", new[] { "Event_Id" });
            DropIndex("dbo.AreaMovement", new[] { "ControlAreaId" });
            DropIndex("dbo.AreaMovement", new[] { "VehicleId" });
            DropTable("dbo.WantedVehicle");
            DropTable("dbo.Vehicle");
            DropTable("dbo.VehicleBrand");
            DropTable("dbo.Region");
            DropTable("dbo.OfficialPlate");
            DropTable("dbo.Plate");
            DropTable("dbo.Event");
            DropTable("dbo.Camera");
            DropTable("dbo.CameraArea");
            DropTable("dbo.ControlArea");
            DropTable("dbo.AreaMovement");
        }
    }
}
