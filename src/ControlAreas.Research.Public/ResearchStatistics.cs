﻿using ControlAreas.Research.Public.ExperimentEntities;

namespace ControlAreas.Research.Public
{
    static public class ResearchStatistics
    {
        private static ExperimentResult _experimentResult = new ExperimentResult();

        public static void NewExperiment(ExperimentSettings settings)
        {
            _experimentResult = new ExperimentResult(settings);
        }

        public static void AddVehicleGenerated()
        {
            _experimentResult.VehiclesGenerated++;
        }

        public static void AddGeneratedEventNotFirst()
        {
            _experimentResult.EventsGenerated++;
        }

        public static void AddFirstAppearance()
        {
            _experimentResult.FirstAppearanceN++;
        }

        public static void AddNotStandart()
        {
            _experimentResult.NotStandartN++;
        }

        public static void AddStandart()
        {
            _experimentResult.StandartN++;
        }

        public static ExperimentResult GetExperimentResults()
        {
            _experimentResult.FirstAppearanceN -= _experimentResult.ExperimentSettings.RealVehiclesN;
            return _experimentResult;
        }
    }
}
