﻿using System.Collections.Generic;

namespace ControlAreas.Research.Public.ExperimentEntities
{
    public class ExperimentSettings
    {
        public double RecognitionError { get; set; }
        public double SkipError { get; set; }
        public int EventsN { get; set; }
        public int RealVehiclesN { get; set; }
        public List<int> CameraIds { get; set; }
    }
}
