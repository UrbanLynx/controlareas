﻿namespace ControlAreas.Research.Public.ExperimentEntities
{
    public class ExperimentResult
    {
        public ExperimentResult(ExperimentSettings settings)
        {
            VehiclesGenerated = 0;
            FirstAppearanceN = 0;
            NotStandartN = 0;
            StandartN = 0;
            EventsGenerated = 0;
            ExperimentSettings = settings;
        }

        public ExperimentResult()
        {
            VehiclesGenerated = 0;
            FirstAppearanceN = 0;
            NotStandartN = 0;
            StandartN = 0;
            EventsGenerated = 0;
            ExperimentSettings = new ExperimentSettings();
        }

        public ExperimentSettings ExperimentSettings { get; set; }

        public int FirstAppearanceN { get; set; }
        public int NotStandartN { get; set; }
        public int StandartN { get; set; }
        public int VehiclesGenerated { get; set; }
        public int EventsGenerated { get;set; }
    }
}
