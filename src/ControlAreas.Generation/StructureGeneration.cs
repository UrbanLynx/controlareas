﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Generation.Entities;

namespace ControlAreas.Generation
{
    public class StructureGeneration
    {
        private ViFlowContext db = new ViFlowContext();


        public void GenerateModel(MapModel model)
        {
            //DeleteDatabase();
            WriteXML(model);
            //GenerateEntities(model);
        }

        public void GenerateModelFromFile()
        {
            var model = ReadXML();
            GenerateEntities(model);
        }

        private void GenerateEntities(MapModel model)
        {
            GenerateRegions(model.regionNumber);
            GenerateControlAreas(model.controlAreas);
            GenerateCameras(model.cameras, model.controlAreas);
            GenerateVehicleBrand(model.brandNumber);
            //GeneratePlates(model.plateNumber);
        }

        public void DeleteDatabase()
        {
            db.Database.Delete();
            db.Database.Create();
        }

        public void WriteXML(MapModel model)
        {
            var writer =new System.Xml.Serialization.XmlSerializer(typeof(MapModel));
            var file = new System.IO.StreamWriter(@"C:\MapModel.xml");
            writer.Serialize(file, model);
            file.Close();
        }

        public MapModel ReadXML()
        {
            var reader = new System.Xml.Serialization.XmlSerializer(typeof(MapModel));
            var file = new System.IO.StreamReader(@"C:\MapModel.xml");
            var model = (MapModel)reader.Deserialize(file);
            return model;
        }

        private void GenerateRegions(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var region = new Region()
                {
                    Code = i.ToString()
                };
                db.Region.Add(region);
            }
            db.SaveChanges();
        }

        public Camera CreateCamera(float latitude, float longitude, int regionCodeId)
        {
            return new Camera()
            {
                Latitude = (float)latitude,
                Longitude = (float)longitude,
                RegionCodeId = regionCodeId
            };
        }

        public CameraArea CreateCameraArea(int cameraId, int fromAreaId, int toAreaId)
        {
            return new CameraArea()
            {
                CameraId = cameraId,
                ToAreaId = fromAreaId,
                FromAreaId = toAreaId
            };
        }


        /*private void GenerateCameras(MapPosition[] cameras, MapCA[] controlAreas)
        {
            using (var db = new ViFlowContext())
            {
                foreach (var mapPosition in cameras)
                {
                    // смотрит на въезжающих
                    var camera1 = CreateCamera(mapPosition.id, (float)mapPosition.latitude, (float)mapPosition.longitude, 0);
                    db.Camera.Add(camera1);

                    // смотрит на выезжающих
                    var camera2 = CreateCamera(mapPosition.id + cameras.Count(), (float)mapPosition.latitude, (float)mapPosition.longitude, 0);
                    db.Camera.Add(camera2);

                    // TODO: временное решение для того, чтобы EF сгенерил id камер
                    db.SaveChanges();

                    var areas = controlAreas.Where(area => area.positions != null && area.positions.Contains(mapPosition.id)).ToList();
                    switch (areas.Count())
                    {
                        case 0: break;
                        case 1:
                            db.CameraArea.Add(CreateCameraArea(camera1.Id, 0, areas[0].id));
                            db.CameraArea.Add(CreateCameraArea(camera2.Id, areas[0].id, 0));
                            break;
                        case 2:
                            db.CameraArea.Add(CreateCameraArea(camera1.Id, areas[1].id, areas[0].id));
                            db.CameraArea.Add(CreateCameraArea(camera2.Id, areas[0].id, areas[1].id));
                            break;
                    }

                }
                db.SaveChanges();
            }
        }*/

        private void GenerateCameras(MapPosition[] cameras, MapCA[] controlAreas)
        {
            foreach (var cameraPosition in cameras)
            {
                var areas =
                    controlAreas.Where(area => area.positions != null && area.positions.Contains(cameraPosition.id))
                        .ToList();
                var areaCount = areas.Count();
                for (int i = 0; i < areaCount; i++)
                {
                    var area = areas.FirstOrDefault();
                    var otherAreas = areas.Where(a => a.id != area.id).ToList();
                    foreach (var otherArea in otherAreas)
                    {
                        CreateCamerasFor(cameraPosition, area.dbId, otherArea.dbId);
                    }
                    areas.Remove(area);
                }

                foreach (var area in areas)
                {
                    var otherAreas = areas.Where(a => a.id != area.id).ToList();
                    foreach (var otherArea in otherAreas)
                    {
                        CreateCamerasFor(cameraPosition,area.dbId,otherArea.dbId);
                    }
                }
                db.SaveChanges();
            }
        }

        private void CreateCamerasFor(MapPosition cameraPosition, int controlAreaAId, int controlAreaBId)
        {
            // смотрит на въезжающих
            var camera1 = CreateCamera((float)cameraPosition.latitude, (float)cameraPosition.longitude, GetFirstRegion().Id);
            db.Camera.Add(camera1);

            // смотрит на выезжающих
            var camera2 = CreateCamera((float)cameraPosition.latitude, (float)cameraPosition.longitude, GetFirstRegion().Id);
            db.Camera.Add(camera2);

            db.SaveChanges();

            db.CameraArea.Add(CreateCameraArea(camera1.Id, controlAreaAId, controlAreaBId));
            db.CameraArea.Add(CreateCameraArea(camera2.Id, controlAreaBId, controlAreaAId));

            
        }

        private Region GetFirstRegion()
        {
            return db.Region.First();
        }

        private void GenerateControlAreas(MapCA[] controlAreas)
        {
            foreach (var area in controlAreas)
            {
                var controlArea = new ControlArea()
                {
                    Id = area.id,
                    CreationTime = DateTime.Now,
                    IsExternal = area.isExternal
                };
                db.ControlArea.Add(controlArea);
                db.SaveChanges();
                area.dbId = controlArea.Id;
            }
        }

        private void GenerateVehicleBrand(int num)
        {
            for (int i = 0; i < num; i++)
            {
                var brand = new VehicleBrand()
                {
                    Id = i,
                    Name = i.ToString()
                };
                db.VehicleBrand.Add(brand);
            }
            db.SaveChanges();
            
        }


        /*public Plate CreatePlate(int regionCodeId)
        {
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var digits = "0123456789";
            

            return new Plate()
            {
                RegionCodeId = regionCodeId,
                N1 = letters[random.Next(letters.Length)].ToString(),
                N2 = digits[random.Next(digits.Length)].ToString(),
                N3 = digits[random.Next(digits.Length)].ToString(),
                N4 = digits[random.Next(digits.Length)].ToString(),
                N5 = letters[random.Next(letters.Length)].ToString(),
                N6 = letters[random.Next(letters.Length)].ToString(),
                N7 = digits[random.Next(digits.Length)].ToString(),
                N8 = digits[random.Next(digits.Length)].ToString(),
                Recognized = true
            };
        }


        private void GeneratePlates(int num)
        {
            using (var db = new ViFlowContext())
            {
                for (int i = 0; i < num; i++)
                {
                    var plate = CreatePlate(0);
                    db.Plate.Add(plate);

                    var official = CreateOfficialPlate(i, 0, plate);
                    db.OfficialPlate.Add(official);
                }
                db.SaveChanges();
            }
        }

        public OfficialPlate CreateOfficialPlate(int id, int vehicleBrandId, Plate plate)
        {
            return new OfficialPlate()
            {
                Id = id,
                PlateId = plate.Id,
                RegistrationRegionId = plate.RegionCodeId,
                RegistrationTime = DateTime.Now,
                VehicleBrandId = vehicleBrandId
            };
        }*/
    }
}
