﻿namespace ControlAreas.Generation.Entities
{
    public class MapPosition
    {
        public int id { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }

    public class MapCA
    {
        public int id { get; set; }
        public int[] positions { get; set; }
        public bool isExternal { get; set; }
        public int dbId { get; set; }
    }

    public class MapModel
    {
        public MapPosition[] cameras { get; set; }
        public MapCA[] controlAreas { get; set; }
        public int regionNumber { get; set; }
        public int brandNumber { get; set; }
        public int plateNumber { get; set; }
        public int officialPlates { get; set; }
    }
}