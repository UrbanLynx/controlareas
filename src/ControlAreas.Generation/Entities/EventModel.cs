﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAreas.Generation.Entities
{
    public class EventProperies
    {
        public EventProperies()
        {
            /*PofFirstAppear = 0.3;
            PofAppearOnBorder = 0.1;
            PofAppearRegistered = 0.9;
            PofStolenVehicle = 0.05;
            PofTwin = 0.05;
            PofChangePlateNotValid = 0.1;
            PofChangePlateReal = 0.1;
            PofChangePlateUnreal = 0.1;
            PofEventSkip = 0.1;*/
            PofFirstAppear = 0.01;
            PofAppearOnBorder = 0.9;
            PofAppearRegistered = 0.95;
            PofStolenVehicle = 0.05;
            PofTwin = 0.05;
            PofChangePlateNotValid = 0.05;
            PofChangePlateReal = 0.05;
            PofChangePlateUnreal = 0.05;
            PofEventSkip = 0;
        }

        public double PofFirstAppear { get; set; }
        public double PofAppearOnBorder { get; set; }
        public double PofAppearRegistered { get; set; }
        public double PofStolenVehicle { get; set; }
        public double PofTwin { get; set; }
        public double PofChangePlateReal { get; set; }
        public double PofChangePlateUnreal { get; set; }
        public double PofChangePlateNotValid { get; set; }
        public double PofEventSkip { get; set; }
        //public double PofNormal { get; set; }
    }

    public class EventModel
    {
        public EventModel()
        {
            Properties = new EventProperies();
            SleepFrom = 100;
            SleepTo = 200;
            MinutesBetweenEvents = 5;
            StartDateTime = new DateTime(2015,1,12);
        }

        //public int plateId { get; set; }
        //public int cameraId { get; set; }
        //public int eventNumber { get; set; }
        //public int sleepTime { get; set; }

        public EventProperies Properties { get; set; }
        public double SleepFrom { get; set; }
        public double SleepTo { get; set; }
        public int MinutesBetweenEvents { get; set; }
        public DateTime StartDateTime { get; set; }
    }
}
