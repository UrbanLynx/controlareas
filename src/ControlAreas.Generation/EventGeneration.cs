﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Generation.Entities;

namespace ControlAreas.Generation
{
    public class EventGeneration
    {
        private Plate _plate;
        private EventModel _model;
        private bool _continueGeneration = true;
        private Random _random = new Random(DateTime.Now.Millisecond);
        private ViFlowContext _db = new ViFlowContext();

        private DbInteraction db = new DbInteraction();
        private List<KeyValuePair<Action, double>> _possibleEvents = new List<KeyValuePair<Action, double>>();

        public EventGeneration(EventModel model)
        {
            _model = model;
            //_plate = _db.Plate.FirstOrDefault(p => p.Id == model.plateId);
            InitModel();
        }

        private void InitModel()
        {
            _possibleEvents = new List<KeyValuePair<Action, double>>
            {
                new KeyValuePair<Action, double>(GenerateTwin, _model.Properties.PofTwin),
                new KeyValuePair<Action, double>(GenerateStolen, _model.Properties.PofStolenVehicle),
                new KeyValuePair<Action, double>(GenerateFirstAppearence, _model.Properties.PofFirstAppear),
                new KeyValuePair<Action, double>(GenerateForVehicleWithHistory, 1)
            };
        }

        public void StartGeneration()
        {
            db.SetCurrentTime(_model.StartDateTime);
            while (_continueGeneration)
            {
                ExecuteRandom(_possibleEvents);
                db.IncreaseCurrentTime(_model.MinutesBetweenEvents);
                Thread.Sleep(GetSleepTime());
            }
        }

        private int GetSleepTime()
        {
            return (int)(_model.SleepFrom + _random.NextDouble() * (_model.SleepTo - _model.SleepFrom));
        }

        public void StopGeneration()
        {
            _continueGeneration = false;
        }

        private void ExecuteRandom(List<KeyValuePair<Action, double>> elements)
        {
            double diceRoll = _random.NextDouble();

            double cumulative = 0.0;
            for (int i = 0; i < elements.Count; i++)
            {
                cumulative += elements[i].Value;
                if (diceRoll < cumulative)
                {
                    elements[i].Key();
                    break;
                }
            }
        }

        private void GenerateTwin()
        {
            var rv = db.ChooseReaOfficiallVehicle();
            if (rv != null && db.CanCreateTwin(rv))
            {
                var rvCopy = db.CreateRealVehicle(rv);
                rvCopy.VehicleBrandId = db.ChooseAnotherBrand(rv);
                db.AddRealVehicle(rvCopy);

                var lastCam = db.GetLastCam(rv);
                var nextCam = db.ChooseNotConnectedCamera(lastCam);
                GenerateEvent(rvCopy, nextCam, false);
            }
            
        }

        private void GenerateStolen()
        {
            var rv = db.ChooseRealVehicle();
            if (rv != null)
            {
                var lastCam = db.GetLastCam(rv);
                var nextCam = db.ChooseNotConnectedCamera(lastCam);
                GenerateEvent(rv, nextCam, false);
            }
        }

        private void GenerateFirstAppearence()
        {
            Camera camera = null;
            var possibleCameras = new List<KeyValuePair<Action, double>>
                {
                    new KeyValuePair<Action, double>(() => camera = db.ChooseFirstCamera(true),_model.Properties.PofAppearOnBorder),
                    new KeyValuePair<Action, double>(() => camera = db.ChooseFirstCamera(false), 1)
                };
            ExecuteRandom(possibleCameras);

            var rv = db.CreateRealVehicle();
            var possibleRegistration = new List<KeyValuePair<Action, double>>
                {
                    new KeyValuePair<Action, double>(() => db.RegisterRealVehicle(rv),_model.Properties.PofAppearRegistered)
                };
            ExecuteRandom(possibleRegistration);

            GenerateEvent(rv,camera,false);
        }

        private void GenerateForVehicleWithHistory()
        {
            var rv = db.ChooseRealVehicle();
            if (rv != null)
            {
                var oldCam = db.GetLastCam(rv);
                var newCam = db.ChooseNextCamera(oldCam);
                GenerateEvent(rv, newCam, true);
            }
        }

        private void GenerateEvent(RealVehicle rv, Camera camera, bool changePlate)
        {
            var newEvent = new ControlAreas.Dbs.Entities.DbEntities.Event()
            {
                CameraId = camera.Id,
                Time = GenerateTime(),
                PlateId = GetPlate(rv,changePlate),
                VehicleBrandlId = GetBrand(rv),
                PhotoId = GetPhoto()
            };
            db.AddEvent(newEvent);

            var newRealEvent = new RealEvent()
            {
                EventId = newEvent.Id,
                RealVehicleId = rv.Id
            };
            db.AddRealEvent(newRealEvent);
        }

        private int? GetBrand(RealVehicle rv)
        {
            return rv.VehicleBrandId;
        }

        private long GetPhoto()
        {
            return 0; // TODO: пока моки
        }

        private DateTime GenerateTime()
        {
            return db.GetCurrentTime();
        }

        private int? GetPlate(RealVehicle rv, bool changePlate)
        {
            var plate = rv.Plate;
            if (changePlate)
            {
                var possibleChanges = new List<KeyValuePair<Action, double>>
                {
                    new KeyValuePair<Action, double>(() => plate = db.ChooseRealPlate(),_model.Properties.PofChangePlateReal),
                    new KeyValuePair<Action, double>(() => plate = db.ChooseUnrealPlate(),_model.Properties.PofChangePlateUnreal),
                    new KeyValuePair<Action, double>(() => plate = db.MakeNotValid(plate),_model.Properties.PofChangePlateNotValid)
                };
                ExecuteRandom(possibleChanges);
                plate = db.AddPlateIfNeeded(plate);
            }
            return plate.Id; // проверить, что после добавления в БД есть Id
        }

        

        
        

        


/*
        public void GenerateEventsFinite()
        {
            var camera = GetFirstCamera();
            CreateEvent(camera);
            for (int i = 0; i < _model.eventNumber; i++)
            {
                var cameras = GetConnectedCameras(camera);
                if (cameras != null)
                {
                    camera = cameras[_random.Next(cameras.Count)];
                    CreateEvent(camera);
                    Thread.Sleep(_model.sleepTime);
                }
            }
        }


        private Camera GetFirstCamera()
        {
            var cameraArea = _db.CameraArea.Where(ca => ca.FromArea.IsExternal).FirstOrDefault();
            if (cameraArea != null)
                return cameraArea.Camera;

            return null;
        }

        

        public void CreateEvent(Camera camera)
        {
            var newEvent = new ControlAreas.Dbs.Entities.DbEntities.Event()
            {
                CameraId = camera.Id,
                Time = GenerateTime(),
                PlateId = GetPlate(),
                VehicleBrandlId = GetBrand(),
                PhotoId = GetPhoto()
            };
            _db.Event.Add(newEvent);
            _db.SaveChanges();
        }

        private long GetPhoto()
        {
            return 0; // TODO: пока моки
        }

        private int? GetBrand()
        {
            return 0; // TODO: выбор по dbo.officialPlate
        }

        private int? GetPlate()
        {
            return _plate.Id;
        }

        private DateTime GenerateTime()
        {
            return DateTime.Now;
        }*/
    }
}
