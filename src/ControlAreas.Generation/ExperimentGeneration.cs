﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Generation.Entities;
using ControlAreas.Research;
using ControlAreas.Research.Public;
using ControlAreas.Research.Public.ExperimentEntities;

namespace ControlAreas.Generation
{
    public class ExperimentGeneration
    {
        private ExperimentSettings _experimentSettings;
        private int _eventsGenerated = 0;

        private EventModel _model;
        private Random _random = new Random(DateTime.Now.Millisecond);

        private DbInteraction db = new DbInteraction();
        private List<KeyValuePair<Action, double>> _possibleEvents = new List<KeyValuePair<Action, double>>();
        private int _vehicleN=0;
        private bool replaced = false;

        public ExperimentGeneration(ExperimentSettings settings)
        {
            _experimentSettings = settings;
            InitModel();
        }

        private void InitModel()
        {
            _model = new EventModel();
            _model.Properties.PofFirstAppear = (double) _experimentSettings.RealVehiclesN/_experimentSettings.EventsN;

            SetCurrentPossibleEvents();
        }


        private void SetCurrentPossibleEvents()
        {
            _possibleEvents = new List<KeyValuePair<Action, double>>
            {
                new KeyValuePair<Action, double>(GenerateFirstAppearence, _model.Properties.PofFirstAppear),
                new KeyValuePair<Action, double>(GenerateForVehicleWithHistory, 1)
            };
        }

        public void GenerateFinite()
        {
            db.SetCurrentTime(_model.StartDateTime);
            GenerateFirstAppearence();
            while (ContinueGeneration())
            {
                var timer = new Stopwatch();
                timer.Start();
                db.IncreaseCurrentTime(_model.MinutesBetweenEvents);
                db.Recreate();
                ExecuteRandom(_possibleEvents);
                timer.Stop();
                //Console.WriteLine("Time of generating evet - {0}", timer.ElapsedMilliseconds);
            }
        }

        private bool ContinueGeneration()
        {
            if (_vehicleN >= _experimentSettings.RealVehiclesN && !replaced)
            {
                _model.Properties.PofFirstAppear = 0;
                SetCurrentPossibleEvents();
                replaced = true;
            }
            return _eventsGenerated < _experimentSettings.EventsN;
        }

        private void ExecuteRandom(List<KeyValuePair<Action, double>> elements)
        {
            double diceRoll = _random.NextDouble();

            double cumulative = 0.0;
            for (int i = 0; i < elements.Count; i++)
            {
                cumulative += elements[i].Value;
                if (diceRoll < cumulative)
                {
                    elements[i].Key();
                    break;
                }
            }
        }

        private void GenerateFirstAppearence()
        {
            Camera camera = db.ChooseFirstCamera(true);

            var rv = db.CreateRealVehicle();
            db.RegisterRealVehicle(rv);

            GenerateEvent(rv,camera,true);

            ResearchStatistics.AddVehicleGenerated();
            _vehicleN++;
        }

        private void GenerateForVehicleWithHistory()
        {
            var timer = new Stopwatch();
            timer.Start();

            var rv = db.ChooseRealVehicle();
            if (rv != null)
            {
                var oldCam = db.GetLastCam(rv);
                var newCam = db.ChooseNextCamera(oldCam);
                GenerateEvent(rv, newCam, true);
            }
            ResearchStatistics.AddGeneratedEventNotFirst();

            timer.Stop();
            //Console.WriteLine("GenerateForVehicleWithHistory - {0}", timer.ElapsedMilliseconds);
        }

        private void GenerateEvent(RealVehicle rv, Camera camera, bool changePlate)
        {
            

            changePlate = false;
            if (_experimentSettings.CameraIds.Contains(camera.Id))
            {
                _eventsGenerated++;
                changePlate = true;
            }

            
            var newEvent = new Event()
            {
                CameraId = camera.Id,
                Time = GenerateTime(),
                PlateId = GetPlate(rv,changePlate),
                VehicleBrandlId = GetBrand(rv),
                PhotoId = GetPhoto()
            };
            
            var timer = new Stopwatch();
            timer.Start();
            db.AddEvent(newEvent);
            timer.Stop();
            //Console.WriteLine("Event - {0}", timer.ElapsedMilliseconds);

            timer.Restart();
            var newRealEvent = new RealEvent()
            {
                EventId = newEvent.Id,
                RealVehicleId = rv.Id
            };
            db.AddRealEvent(newRealEvent);

            timer.Stop();
            //Console.WriteLine("RealEvent - {0}", timer.ElapsedMilliseconds);
        }

        private int? GetBrand(RealVehicle rv)
        {
            return rv.VehicleBrandId;
        }

        private long GetPhoto()
        {
            return 0; // TODO: пока моки
        }

        private DateTime GenerateTime()
        {
            return db.GetCurrentTime();
        }

        private int? GetPlate(RealVehicle rv, bool changePlate)
        {
            var plate = rv.Plate;
            if (changePlate)
            {
                var possibleChanges = new List<KeyValuePair<Action, double>>
                {
                    new KeyValuePair<Action, double>(() => plate = db.ChooseUnrealPlate(),_experimentSettings.RecognitionError),
                    new KeyValuePair<Action, double>(() => plate = db.MakeNotValid(plate),_experimentSettings.SkipError),
                    new KeyValuePair<Action, double>(() => plate = plate, 1)
                };
                ExecuteRandom(possibleChanges);
                plate = db.AddPlateIfNeeded(plate);
            }
            return plate.Id; // проверить, что после добавления в БД есть Id
        }
    }
}
