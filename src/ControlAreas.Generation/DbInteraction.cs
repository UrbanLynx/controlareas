﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;
using System.Data.Entity;

namespace ControlAreas.Generation
{
    public class DbInteraction
    {
        private ViFlowContext _db = new ViFlowContext();
        private Random _random = new Random(DateTime.Now.Millisecond);

        private DateTime currentTime = new DateTime();

        public void SetCurrentTime(DateTime time)
        {
            currentTime = time;
        }

        public DateTime GetCurrentTime()
        {
            return currentTime;
        }

        public DateTime IncreaseCurrentTime(int minutes)
        {
            currentTime = currentTime.AddMinutes(minutes);
            return currentTime;
        }

        public RealVehicle ChooseRealVehicle()
        {
            var timer = new Stopwatch();
            timer.Start();

            var vehicles = _db.RealVehicle.ToList();
            if (vehicles.Any())
            {
                var e = vehicles[_random.Next(vehicles.Count)];

                timer.Stop();
                //Console.WriteLine("ChooseRealVehicle - {0}", timer.ElapsedMilliseconds);
                return e;
            }
            return null;
        }

        public RealVehicle ChooseReaOfficiallVehicle()
        {
            var vehicles = _db.RealVehicle.Where(v => v.OfficialPlatedId != null).ToList();
            if (vehicles.Any())
            {
                return vehicles[_random.Next(vehicles.Count)];
            }
            return null;
        }

        public Camera GetLastCam(RealVehicle rv)
        {
            

                var timer = new Stopwatch();
                timer.Start();

                var e = _db.RealEvent.AsNoTracking().Include(m => m.Event).Where(r => r.RealVehicleId == rv.Id)
                    .OrderByDescending(r => r.Event.Time).FirstOrDefault().Event.Camera;

                //var e = context.Event.Find(lastEvent.EventId).Camera;

                timer.Stop();
                //Console.WriteLine("GetLastCam - {0}", timer.ElapsedMilliseconds);

                return e;
            
            /*_db.RealEvent.Where(r => r.RealVehicleId == rv.Id)
                    .OrderByDescending(r => r.Event.Time)
                    .First()
                    .Event.Camera;*/
        }

        public void AddEvent(Event newEvent)
        {
            using (var context = new ViFlowContext())
            {
                context.Event.Add(newEvent);
                context.SaveChanges();   
            }
        }

        public void AddRealEvent(RealEvent newRealEvent)
        {
            using (var context = new ViFlowContext())
            {
                context.RealEvent.Add(newRealEvent);
                context.SaveChanges();
            }
        }

        public Plate ChooseRealPlate()
        {
            return ChooseRealVehicle().Plate; // может выбрать то же ТС, для которого мы хотим поменять ГРЗ
        }

        public RealVehicle CreateRealVehicle()
        {
            var plate = ChooseUnrealPlate();
            _db.Plate.Add(plate);

            var brand = ChooseBrand();
            _db.SaveChanges();

            var rv = new RealVehicle()
            {
                PlateId = plate.Id,
                VehicleBrandId = brand.Id
            };
            _db.RealVehicle.Add(rv);
            _db.SaveChanges();
            return rv;
        }

        public void RegisterRealVehicle(RealVehicle rv)
        {
            var region = ChooseRegion();
            var time = ChooseTime();
            rv.OfficialPlate = new OfficialPlate()
            {
                PlateId = rv.PlateId,
                RegistrationRegionId = region.Id,
                VehicleBrandId = rv.VehicleBrandId,
                RegistrationTime = time
            };
            _db.OfficialPlate.Add(rv.OfficialPlate);
            _db.SaveChanges();

            /*var official = new OfficialPlate()
            {
                PlateId = rv.PlateId,
                RegistrationRegionId = region.Id,
                VehicleBrandId = rv.VehicleBrandId,
                RegistrationTime = time
            };
            _db.OfficialPlate.Add(official);
            _db.SaveChanges();

            rv.OfficialPlatedId = official.Id;
            _db.SaveChanges();*/
        }

        private DateTime ChooseTime()
        {
            return GetCurrentTime();
            //return DateTime.Now;
        }

        private VehicleBrand ChooseBrand()
        {
            var brands = _db.VehicleBrand.ToList();
            return brands[_random.Next(brands.Count)];
        }

        private Region ChooseRegion()
        {
            var regions = _db.Region.ToList();
            return regions[_random.Next(regions.Count)];
        }

        public Plate ChooseUnrealPlate()
        {
            var found = false;
            Plate p;
            do
            {
                p = CreatePlate(ChooseRegion().Id); // поменять код региона
                found =
                    _db.Plate.AsEnumerable().Any(d => ComparePlates(d,p));
            } while (found);
            return p;
        }

        public Plate MakeNotValid(Plate plate)
        {
            var newPlate = CreatePlate(plate);
            newPlate.N1 = null;
            newPlate.Recognized = false;
            _db.Plate.Add(newPlate);
            _db.SaveChanges();
            return newPlate;
        }

        public bool ComparePlates(Plate d, Plate p)
        {
            return d.N1 == p.N1 && d.N2 == p.N2 && d.N3 == p.N3 && d.N4 == p.N4 && d.N5 == p.N5 && d.N6 == p.N6 &&
                   d.N7 == p.N7 && d.N8 == p.N8 && d.N9 == p.N9 && d.N10 == p.N10;
        }

        public Plate CreatePlate(int regionCodeId)
        {
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var digits = "0123456789";


            return new Plate()
            {
                RegionCodeId = regionCodeId,
                N1 = letters[_random.Next(letters.Length)].ToString(),
                N2 = digits[_random.Next(digits.Length)].ToString(),
                N3 = digits[_random.Next(digits.Length)].ToString(),
                N4 = digits[_random.Next(digits.Length)].ToString(),
                N5 = letters[_random.Next(letters.Length)].ToString(),
                N6 = letters[_random.Next(letters.Length)].ToString(),
                N7 = digits[_random.Next(digits.Length)].ToString(),
                N8 = digits[_random.Next(digits.Length)].ToString(),
                Recognized = true
            };
        }

        public Plate CreatePlate(Plate plate)
        {
            return new Plate()
            {
                RegionCodeId = plate.RegionCodeId,
                N1 = plate.N1,
                N2 = plate.N2,
                N3 = plate.N3,
                N4 = plate.N4,
                N5 = plate.N5,
                N6 = plate.N6,
                N7 = plate.N7,
                N8 = plate.N8,
                N9 = plate.N9,
                N10 = plate.N10,
                Recognized = true
            };
        }

        public Plate AddPlateIfNeeded(Plate plate)
        {
            // возможно неполное сравнение - нет проверки IsRecognized и мб еще что то
            var plateExist = _db.Plate.AsEnumerable().FirstOrDefault(d => ComparePlates(d, plate));
            if (plateExist == null)
            {
                plate = CreatePlate(plate);
                _db.Plate.Add(plate);
                _db.SaveChanges();
                return plate;
            }
            return plateExist;
        }

        public Camera ChooseFirstCamera(bool onBorder)
        {
            var cameraAreas = _db.CameraArea.Where(c => c.FromArea.IsExternal == onBorder).ToList();
            return cameraAreas[_random.Next(cameraAreas.Count())].Camera;
        }

        public Camera ChooseNextCamera(Camera camera)
        {
            var timer = new Stopwatch();
            timer.Start();

            var cameras = GetConnectedCamerasWithoutFromArea(camera);



            timer.Stop();
            //Console.WriteLine("ChooseNextCamera - {0}", timer.ElapsedMilliseconds);
            var e = cameras[_random.Next(cameras.Count)];

            

            return e;
        }

        private List<Camera> GetConnectedCamerasWithoutFromArea(Camera camera)
        {
            using (var context = new ViFlowContext())
            {
                var cameraArea = camera.CameraArea.FirstOrDefault();
                if (cameraArea != null)
                {
                    return
                        context.CameraArea.Where(
                            ca => ca.FromAreaId == cameraArea.ToAreaId && ca.ToAreaId != cameraArea.FromAreaId)
                            .Select(ca => ca.Camera)
                            .ToList();
                }

                return null;
            }
        } 


        private List<Camera> GetConnectedCameras(Camera camera)
        {
            using (var context = new ViFlowContext())
            {
                var cameraArea = camera.CameraArea.FirstOrDefault();
                if (cameraArea != null)
                {
                    return context.CameraArea.Where(ca => ca.FromAreaId == cameraArea.ToAreaId).Select(ca => ca.Camera).ToList();
                }

                return null;
            }
            
        }

        public Camera ChooseNotConnectedCamera(Camera camera)
        {
            var connectedCameras = GetConnectedCameras(camera);
            var notConnectedCameras = _db.Camera.AsEnumerable().Where(c => connectedCameras.All(con => con.Id != c.Id)).ToList();
            return notConnectedCameras[_random.Next(notConnectedCameras.Count())];
        }

        public RealVehicle CreateRealVehicle(RealVehicle rv)
        {
            return new RealVehicle
            {
                PlateId = rv.PlateId,
                VehicleBrandId = rv.VehicleBrandId
            };
        }

        public int ChooseAnotherBrand(RealVehicle rv)
        {
            var brands = _db.VehicleBrand.Where(b => b.Id != rv.VehicleBrandId).ToList();
            return brands[_random.Next(brands.Count())].Id;
        }

        public void AddRealVehicle(RealVehicle rvCopy)
        {
            _db.RealVehicle.Add(rvCopy);
            _db.SaveChanges();
        }

        public void Recreate()
        {
            _db = new ViFlowContext();
        }

        public bool CanCreateTwin(RealVehicle rv)
        {
            var twins = _db.RealVehicle.Count(v => v.PlateId == rv.PlateId);
            var brands = _db.VehicleBrand.Count();
            if (twins < brands)
            {
                return true;
            }
            return false;
        }

        
    }
}
