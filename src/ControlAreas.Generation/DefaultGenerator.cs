﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Generation
{
    class DefaultGenerator
    {
        private ViFlowContext db = new ViFlowContext();

        public void GenerateStructure()
        {
            GenerateRegions();
            GenerateControlAreas();
            GenerateCameras();
            GenerateVehicleBrand();
            GeneratePlates();
        }

        public void GenerateEvents()
        {

        }

        private void GenerateRegions()
        {
            throw new NotImplementedException();
        }

        private void GenerateControlAreas()
        {
            throw new NotImplementedException();
        }

        private void GenerateCameras()
        {
            throw new NotImplementedException();
        }

        private void GenerateVehicleBrand()
        {
            throw new NotImplementedException();
        }

        private void GeneratePlates()
        {
            throw new NotImplementedException();
        }


        
    }
}
