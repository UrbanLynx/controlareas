﻿
$(function () {
    initWantedTable();
    initDatetimepicker();
});

var selectedVhicleId;

function populateWanted() {
    $.ajax({
        url: "GetAllWantedVehicles",
        type: 'GET',
        contentType: 'application/json',
        success: fillWantedVehiclesTable,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function fillWantedVehiclesTable(options) {
        $('#wantedVehiclesTable').bootstrapTable('load', options);
        setTableStyle();
    }
}

function getLastControlArea(vehicleId) {
    $.ajax({
        url: "GetLastControlArea",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ VehicleId: vehicleId }),
        success: ajaxOnSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function ajaxOnSuccess(data) {
        clearAllFigures();
        drawControlArea(data, true);
    }
}

function fillWantedVehiclesTable(options) {
    $('#wantedVehiclesTable').bootstrapTable('load', options);
    setTableStyle();
}

function initWantedTable() {
    $('#wantedVehiclesTable').bootstrapTable({
        columns: [
        {
            field: 'State',
            radio: 'true'
        }, {
            field: 'VehicleId',
            title: 'ID'
        }, {
            field: 'Plate',
            title: 'ГРЗ'
        }, {
            field: 'Reason',
            title: 'Причина'
        }],
        data: [],
        clickToSelect: true,
        selectItemName: 'radioWanted',
        formatNoMatches: function () { return ''; }
    })
    .on('click-row.bs.table', function (e, row, $element) {
        selectedVhicleId = row.Id;
        getLastControlArea(row.VehicleId);
    });

    setTableStyle();
}