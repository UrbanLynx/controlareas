﻿
$(function () {
    initAreaListener();
    initAreaTable();
    initDatetimepicker();
});

var selectedControlAreaId;

function initAreaListener() {
    $(window).on('polygonClicked', function (e) {
        clearAllFigures();
        changeControlArea(e.controlAreaId);
    });
} 

function populateAreas() {
    $.ajax({
        url: "GetAllControlAreas",
        type: 'GET',
        contentType: 'application/json',
        success: drawAllControlAreas,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function drawAllControlAreas(data) {
        clearAllFigures();
        data.forEach(drawControlArea);
    }

    
}

function changeControlArea(areaId) {
    $.ajax({
        url: "GetAreaMap",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ AreaId: areaId }),
        success: ajaxOnSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function ajaxOnSuccess(data) {
        selectedControlAreaId = areaId;
        drawControlArea(data, true);
    }
}

function getControlAreaDetails() {
    var fromTime = moment($('#infoFromTime').val(), momentTimeFormat);
    var toTime = moment($('#infoToTime').val(), momentTimeFormat);

    $.ajax({
        url: "GetControlAreaDetails",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ AreaId: selectedControlAreaId, FromTime:fromTime, ToTime:toTime }),
        success: ajaxOnSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function ajaxOnSuccess(data) {
        fillVehicleInCaTable(data);
    }
}

function fillVehicleInCaTable(options) {
    $('#vehicleInCaTable').bootstrapTable('load', options);
    setTableStyle();
}

function initAreaTable() {
    $('#vehicleInCaTable').bootstrapTable({
        columns: [
        {
            field: 'State',
            radio: 'true'
        }, {
            field: 'Id',
            title: 'ID'
        }, {
            field: 'Plate',
            title: 'ГРЗ'
        }, {
            field: 'FromTime',
            title: 'С'
        }, {
            field: 'ToTime',
            title: 'По'
        }, {
            field: 'Visits',
            title: 'Посещения'
        }],
        data: [],
        clickToSelect: true,
        selectItemName: 'radioArea',
        formatNoMatches: function () { return ''; }
    })
    .on('click-row.bs.table', function (e, row, $element) {
        
    });

    setTableStyle();
}