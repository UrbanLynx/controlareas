﻿

function initDatetimepicker() {
    $(".form_datetime").datetimepicker({
        format: bootstrapTimeFormat,
        autoclose: true,
        todayBtn: true,
        minuteStep: 2,
        weekStart: 1,
        language: 'ru'
    });
}

function setTableStyle() {
    $('td').each(function () {
        this.style.setProperty('padding', '2px', 'important');
    });
    //$('td').css('padding', '2px');
    //$('td').addClass('cell-xs');
    //$('tr').addClass('cell-xs');
    //$('table').addClass('cell-xs');
}

var bootstrapTimeFormat = "dd/mm/yyyy hh:ii";
var momentTimeFormat = "DD/MM/YYYY HH:mm";
var cameraInColor = "";
var cameraOutColor = "";