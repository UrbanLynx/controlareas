﻿
var map;
var allFigures = [];

var everythingElse = [
    new google.maps.LatLng(17, 0),
    new google.maps.LatLng(80, 0),
    new google.maps.LatLng(80, 170),
    new google.maps.LatLng(17, 170),
    new google.maps.LatLng(17, 0)];

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(55.74180, 37.59383),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"),
        mapOptions);
}

function drawControlArea(controlArea, addCameras) {
    var polygonPoints = [];
    controlArea.Cameras.forEach(function (cam) {
        polygonPoints.push(new google.maps.LatLng(cam.Lat, cam.Lng));
    });
    polygonPoints = orderPoints(polygonPoints);

    if (controlArea.IsExternal == 'False') {
        var currentPoly = drawPolygon(polygonPoints);
        addListenersOnPolygon(currentPoly, controlArea.ControlAreaId);
    } else {
        drawPolyline(polygonPoints);
    }

    if (addCameras) {
        //drawCameras(controlArea.InCameras, inColor);
        //drawCameras(controlArea.OutCameras, outColor);
    }
}

function drawPolygon(polygonPoints) {

    var currentPolygon = new google.maps.Polygon({
        paths: polygonPoints,
        strokeColor: '#002560',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#AF0500',
        fillOpacity: 0.35
    });

    currentPolygon.setMap(map);
    allFigures.push(currentPolygon);

    return currentPolygon;
}

function drawExternalPolygon(polygonPoints) {
    clearAllFigures();
    var currentPolygon = new google.maps.Polygon({
        paths: [everythingElse, polygonPoints],
        strokeColor: '#002560',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#AF0500',
        fillOpacity: 0.35
    });

    currentPolygon.setMap(map);
    allFigures.push(currentPolygon);
}

function drawPolyline(polygonPoints) {
    
    var currentPolygon = new google.maps.Polyline({
        path: polygonPoints,
        strokeColor: '#0000FF',
        strokeOpacity: 0.8,
        strokeWeight: 2
    });

    currentPolygon.setMap(map);
    allFigures.push(currentPolygon);
}

function clearAllFigures() {
    allFigures.forEach(clearFigure);
}

function clearFigure(figure) {
    if (figure != null) {
        figure.setMap(null);
    }
}

function orderPoints(polygonPoints) {
    var convexHull = new ConvexHullGrahamScan();

    //add points
    //(needs to be done for each point, a foreach
    //loop on the input array can be used.)
    polygonPoints.forEach(function (item) {
        convexHull.addPoint(item.lng(), item.lat());
    });

    var hullPoints = convexHull.getHull();
    hullPoints.push(hullPoints[0]);

    hullPoints = hullPoints.map(function (item) {
        return new google.maps.LatLng(item.y, item.x);
    });
    
    return hullPoints;
}

function addListenersOnPolygon(polygon, id) {
    google.maps.event.addListener(polygon, 'click', function (data) {
        var evt = $.Event('polygonClicked');
        evt.controlAreaId = id;
        $(window).trigger(evt);
    });
}


/*for (var i = 0; i < polygonArray.length; i++) {
    //Add the polygon
    var p = new google.maps.Polygon({
        paths: polygonArray[i],
        strokeWeight: 0,
        fillColor: '#FF0000',
        fillOpacity: 0.6,
        indexID: i
    });
    p.setMap(map);
    addListenersOnPolygon(p);
}*/

initialize();

