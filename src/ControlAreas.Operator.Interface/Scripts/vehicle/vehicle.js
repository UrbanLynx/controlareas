﻿
$(function () {
    initVehicleTable();
    initHabitatTable();
    initHistoryTable();
    initDatetimepicker();
});

var selectedVhicleId;

function populateVehicle() {
    var plateText = $('#plateText').val();
    $.ajax({
        url: "GetVehicles",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ PlateText: plateText }),
        success: fillVehicleTable,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function fillVehicleTable(options) {
        $('#vehicleTable').bootstrapTable('load', options);
        setTableStyle();
    }
}

function changeVehicle(vehicleId) {
    getHabitat(vehicleId);
    getHistory(vehicleId);
}

function showAreaOnTime() {
    var time = moment($('#positionTime').val(), momentTimeFormat);
    
    $.ajax({
        url: "GetMovementsOnTime",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ VehicleId: selectedVhicleId, Time: time.toDate() }),
        success: onAjaxSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function onAjaxSuccess(data) {
        clearAllFigures();
        data.forEach(function(area) {
            changeControlArea(area.AreaId);
        });

    }
}

function getHabitat(vehicleId) {
    $.ajax({
        url: "GetHabitatContolAreas",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ VehicleId: vehicleId }),
        success: fillHabitatTable,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function fillHabitatTable(options) {
        $('#habitatTable').bootstrapTable('load', options);
        setTableStyle();
    }
}

function getHistory(vehicleId) {
    $.ajax({
        url: "GetHistoryContolAreas",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ VehicleId: vehicleId }),
        success: fillHistoryTable,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function fillHistoryTable(options) {
        $('#historyTable').bootstrapTable('load', options);
        setTableStyle();
    }
}

function changeControlArea(areaId) {
    $.ajax({
        url: "GetAreaMap",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({ AreaId: areaId }),
        success: ajaxOnSuccess,
        error: function (jqXHR, exception) {
            alert('Error message.');
        }
    });

    function ajaxOnSuccess(data) {
        drawControlArea(data);
    }
}



function initVehicleTable() {
    $('#vehicleTable').bootstrapTable({
        columns: [
        {
            field: 'State',
            radio: 'true'
        }, {
            field: 'Id',
            title: 'ID'
        }, {
            field: 'Plate',
            title: 'ГРЗ'
        }],
        data: [],
        clickToSelect: true,
        selectItemName: 'radioVehicle',
        formatNoMatches: function () { return ''; }
    })
    .on('click-row.bs.table', function (e, row, $element) {
        selectedVhicleId = row.Id;
        changeVehicle(row.Id);
    });

    setTableStyle();
}

function initHabitatTable() {
    $('#habitatTable').bootstrapTable({
        columns: [
        {
            field: 'State',
            radio: 'true'
        }, {
            field: 'Id',
            title: 'ID'
        }, {
            field: 'Visits',
            title: 'Посещения'
        }],
        data: [],
        clickToSelect: true,
        selectItemName: 'radioArea',
        formatNoMatches: function () { return ''; }
    })
    .on('click-row.bs.table', function (e, row, $element) {
        clearAllFigures();
        changeControlArea(row.Id);
    });

    setTableStyle();
}

function initHistoryTable() {
    $('#historyTable').bootstrapTable({
        columns: [
        {
            field: 'State',
            radio: 'true'
        }, {
            field: 'Id',
            title: 'ID'
        }, {
            field: 'FromTime',
            title: 'С'
        }, {
            field: 'ToTime',
            title: 'По'
        }],
        data: [],
        clickToSelect: true,
        selectItemName: 'radioArea',
        formatNoMatches: function () { return ''; }
    })
    .on('click-row.bs.table', function (e, row, $element) {
        clearAllFigures();
        changeControlArea(row.Id);
    });

    setTableStyle();
}

