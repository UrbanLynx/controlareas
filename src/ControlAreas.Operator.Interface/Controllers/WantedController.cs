﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlAreas.Operator.Interface.Models;
using ControlAreas.Operator.Interface.Server;

namespace ControlAreas.Operator.Interface.Controllers
{
    public class WantedController : Controller
    {
        private InteractionManager _interactionManager = new InteractionManager();
        // GET: Wanted
        public ActionResult IndexWanted()
        {
            return View();
        }

        public JsonResult GetAllWantedVehicles()
        {
            var wanted = _interactionManager.GetAllWantedVehicles();
            return Json(wanted.Select(w => new
            {
                Id = w.Id,
                VehicleId = w.VehicleId,
                Plate = _interactionManager.VehicleToString(w.Vehicle),
                Reason = w.Reason
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLastControlArea(VehicleProperty property)
        {
            var area = _interactionManager.GetLastControlAreaFor(property.VehicleId);
            return Json(_interactionManager.ControlAreaMapToObjectForJson(area), JsonRequestBehavior.AllowGet);
        }
    }
}