﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ControlAreas.Operator.Interface.Models;
using ControlAreas.Operator.Interface.Server;

namespace ControlAreas.Operator.Interface.Controllers
{
    public class AreaController : Controller
    {
        private InteractionManager _interactionManager = new InteractionManager();

        public ActionResult IndexArea()
        {
            return View();
        }

        public JsonResult GetAllControlAreas()
        {
            var areas = _interactionManager.GetAllControlAreas();
            return Json(areas.Select(a => _interactionManager.ControlAreaMapToObjectForJson(a)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAreaMap(ControlAreaProperty property)
        {
            var controlAreaMap = _interactionManager.GetControlAreaMapFor(property.AreaId);
            return Json(_interactionManager.ControlAreaMapToObjectForJson(controlAreaMap), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetControlAreaDetails(ControlAreaProperty property)
        {
            var areaMovements = _interactionManager.GetControlAreaMovements(property);
            return Json(areaMovements.Select(m => new
            {
                Id = m.Id,
                Plate = _interactionManager.VehicleToString(m.Vehicle),
                FromTime = _interactionManager.GetEventTime(m.FromEvent),
                ToTime = _interactionManager.GetEventTime(m.ToEvent),
                Visits = _interactionManager.GetHabitatFor((int) m.VehicleId,m.ControlAreaId).VisitNumber
            }), JsonRequestBehavior.AllowGet);
        }
    }
}