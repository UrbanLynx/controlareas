﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Operator.Interface.Models;
using ControlAreas.Operator.Interface.Server;
using Newtonsoft.Json;

namespace ControlAreas.Operator.Interface.Controllers
{
    public class VehicleController : Controller
    {
        private InteractionManager _interactionManager = new InteractionManager();

        public ActionResult IndexVehicle()
        {
            return View();
        }

        /*[HttpPost]
        public JsonResult GetVehicles(VehicleProperty property)
        {
            var vehicles = _interactionManager.GetAllVehicles();
            return Json(vehicles.Select(v => new
            {
                Id = v.Id.ToString(),
                Plate = _interactionManager.VehicleToString(v)
            }), JsonRequestBehavior.AllowGet);
        }*/

        [HttpPost]
        public JsonResult GetVehicles(VehicleProperty property)
        {
            var vehicles = _interactionManager.GetVehicleByPlate(property.PlateText);
            return Json(vehicles.Select(v => new
            {
                Id = v.Id.ToString(),
                Plate = _interactionManager.VehicleToString(v)
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMovementsOnTime(VehicleProperty property)
        {
            var movements = _interactionManager.GetMovementsOnTime(property.VehicleId, property.Time);
            return Json(movements.Select(a => new
            {
                AreaId = a.ControlAreaId.ToString(),
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetHabitatContolAreas(VehicleProperty property)
        {
            //var areas = _interactionManager.GetAreaMovementsFor(VehicleId);
            var areas = _interactionManager.GetHabitatFor(property.VehicleId);
            return Json(areas.Select(a => new
            {
                Id = a.ControlArea.Id.ToString(),
                Visits = a.VisitNumber.ToString()
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetHistoryContolAreas(VehicleProperty property)
        {
            var areas = _interactionManager.GetAreaMovementsFor(property.VehicleId);
            return Json(areas.Select(a => new
            {
                Id = a.ControlAreaId.ToString(),
                FromTime = _interactionManager.GetEventTime(a.FromEvent),
                ToTime = _interactionManager.GetEventTime(a.ToEvent)
            }), JsonRequestBehavior.AllowGet);
        }

        

        [HttpPost]
        public JsonResult GetAreaMap(ControlAreaProperty property)
        {
            var controlAreaMap = _interactionManager.GetControlAreaMapFor(property.AreaId);
            return Json( _interactionManager.ControlAreaMapToObjectForJson(controlAreaMap),JsonRequestBehavior.AllowGet);
        }

        
    }
}