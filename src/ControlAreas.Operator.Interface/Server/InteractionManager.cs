﻿using System;
using System.Collections.Generic;
using System.Linq;
using ControlAreas.Dbs;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Operator.Interface.Models;
using Microsoft.Owin.Security.Provider;

namespace ControlAreas.Operator.Interface.Server
{
    public class InteractionManager
    {
        private DbManager _dbManager = new DbManager(new ViFlowContext());

        public List<Vehicle> GetAllVehicles()
        {
            return _dbManager.MovementDb.GetAllVehicles();
        }

        public List<AreaMovement> GetAreaMovementsFor(int vehicleId)
        {
            return _dbManager.MovementDb.GetAreaMovementsFor(vehicleId);
        }

        public ControlAreaMap GetControlAreaMapFor(int controlAreaId)
        {
            var controlAreaMap = _dbManager.ControlAreaDb.GetCamerasFor(controlAreaId);
            return controlAreaMap;
        }

        public List<HabitatControlArea> GetHabitatFor(int vehicleId)
        {
            return _dbManager.MovementDb.GetHabitat(vehicleId);
        }

        public HabitatControlArea GetHabitatFor(int vehicleId, int areaId)
        {
            return _dbManager.MovementDb.GetHabitat(vehicleId, areaId);
        }

        public bool IsAreaExternal(int areaId)
        {
            return _dbManager.ControlAreaDb.IsAreaExternal(areaId);
        }

        public List<AreaMovement> GetMovementsOnTime(int vehicleId, DateTime time)
        {
            return _dbManager.MovementDb.GetMovementsOnTime(vehicleId, time);
        }

        public List<ControlAreaMap> GetAllControlAreas()
        {
            return _dbManager.ControlAreaDb.GetAllControlAreaMap();
        }

        public List<AreaMovement> GetControlAreaMovements(ControlAreaProperty property)
        {
            return _dbManager.MovementDb.GetControlAreaMovements(property.AreaId, property.FromTime,
                property.ToTime);
        }

        public string VehicleToString(Vehicle vehicle)
        {
            var p = vehicle.Plate;
            return p.N1 + p.N2 + p.N3 + p.N4 + p.N5 + p.N6 + p.N7 + p.N8 + p.N9 + p.N10;
        }

        public string GetEventTime(Event eventData)
        {
            if (eventData != null)
            {
                return eventData.Time.ToString("g");
            }
            return "-";
        }

        public List<WantedVehicle> GetAllWantedVehicles()
        {
            return _dbManager.MovementDb.GetAllWantedVehicles();
        }

        public ControlAreaMap GetLastControlAreaFor(int vehicleId)
        {
            return _dbManager.ControlAreaDb.GetLastControlAreaFor(vehicleId);
        }

        public object ControlAreaMapToObjectForJson(ControlAreaMap controlAreaMap)
        {
            return new
            {
                ControlAreaId = controlAreaMap.ControlArea.Id.ToString(),
                IsExternal = controlAreaMap.ControlArea.IsExternal.ToString(),
                Cameras = controlAreaMap.Cameras.Select(c => new
                {
                    Id = c.Id.ToString(),
                    Lat = c.Latitude,
                    Lng = c.Longitude
                })
            };
        }

        public List<Vehicle> GetVehicleByPlate(string plate)
        {
            plate = FixPlate(plate);
            return _dbManager.MovementDb.GetVehicleByPlate(plate);
        }

        public string FixPlate(string plate)
        {
            if (plate == null)
            {
                plate = "-";
            }
            var pad = '-';
            var plateLength = 10;
            return plate.PadRight(plateLength, pad);
        }
    }
}