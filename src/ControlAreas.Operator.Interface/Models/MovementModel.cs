﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace ControlAreas.Operator.Interface.Models
{
    public class ItemList
    {
        [DisplayName("Items")]
        public string[] SelectedItemIds { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
    }

    public class VehicleProperty
    {
        public int VehicleId { get; set; }
        public DateTime Time { get; set; }
        public string PlateText { get; set; }
    }

    public class ControlAreaProperty
    {
        public int AreaId { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
    }

    public class AreaMovementProperty
    {
        public int VehicleId { get; set; }
    }

    public class MovementModel
    {
        public ItemList Vehicles { get; set; }
        public ItemList Movements { get; set; }
    }
}