﻿using System.Collections.Generic;

namespace ControlAreas.Operator.Interface.Models
{
    public class MapPoint
    {
        public double latitude { get; set; }
        public double longitude { get; set; } 
    }
    public class AreaMovementMap
    {
        public List<MapPoint> Points { get; set; }
    }
}