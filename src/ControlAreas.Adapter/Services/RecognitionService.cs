﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Adapter.Services
{
    public class RecognitionService
    {
        private ViFlowContext db;
        private double _pError = 0.2;
        private Random _random = new Random(DateTime.Now.Millisecond);

        public RecognitionService(ViFlowContext viFlowContext)
        {
            db = viFlowContext;
        }

        public void Initialize()
        {
        }

        public VehicleBrand RecognizeBrand(EventData eventData)
        {
            // MOC из БД
            var re = db.RealEvent.Single(r => r.EventId == eventData.Event.Id);
            var diceRoll = _random.NextDouble();
            if (diceRoll < _pError)
            {
                return db.VehicleBrand.Find(eventData.Event.VehicleBrandlId);
            }
            return db.RealVehicle.Find(re.RealVehicleId).VehicleBrand;
            //return db.RealEvent.Single(r => r.EventId == eventData.Event.Id).RealVehicle.VehicleBrand;
        }

        public Plate RecognizePlate(EventData eventData)
        {
            //db.SaveChanges();
            var re = db.RealEvent.Single(r => r.EventId == eventData.Event.Id);
            var diceRoll = _random.NextDouble();
            if (diceRoll < _pError)
            {
                return db.Plate.Find(eventData.Event.PlateId);
            }
            return db.RealVehicle.Find(re.RealVehicleId).Plate;
            // распознать ГРЗ, найти это новое ГРЗ в БД и вернуть эту запись, если его нет в БД, то создать
            throw new NotImplementedException();
        }
    }
}
