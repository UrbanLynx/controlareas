﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Dbs.Entities;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Adapter.Services
{
    public class VehicleInfoProvider
    {
        private ViFlowContext db;

        public VehicleInfoProvider(ViFlowContext viFlowContext)
        {
            db = viFlowContext;
        }

        public VehicleOfficialInfo GetPlateInfo(Plate plate)
        {
            // не круто захардкожено
            var official = db.OfficialPlate.SingleOrDefault(o => o.PlateId == plate.Id);
            return new VehicleOfficialInfo {OfficialPlate = official};
        }
    }
}
