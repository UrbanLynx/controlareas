﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlAreas.Dbs;
using ControlAreas.Dbs.Entities.DbEntities;
using ControlAreas.Research.Public.ExperimentEntities;

namespace ControlAreas.Adapter.Services
{
    public class EventProvider
    {
        protected DbManager _dbs;
        protected Action<Event> _eventProcessor;
        private bool _monitorDb = true;
        private int _sleepTime = 1000;

        public EventProvider(ViFlowContext viFlowContext)
        {
            _dbs = new DbManager(viFlowContext);
        }

        public void Initialize()
        {
        }

        public void AddEventProcessor(Action<Event> processor)
        {
            _eventProcessor = processor;
        }

        public void StartMonitorEvents()
        {
            var prevDate = DateTime.Now;
            while (_monitorDb)
            {
                Thread.Sleep(_sleepTime);
                var curDate = DateTime.Now;
                var events = _dbs.EventDb.GetEvents(prevDate, curDate);
                ProcessNewEvents(events);
                prevDate = curDate;
            }
            
        }

        public void StartMonitorEventsByIds()
        {
            var lastId = 0;
            while (_monitorDb)
            {
                Thread.Sleep(_sleepTime);
                var events = _dbs.EventDb.GetOldEvents(lastId);
                ProcessNewEvents(events);

                var lastOrDefault = events.LastOrDefault();
                if (lastOrDefault != null) lastId = (int) lastOrDefault.Id;
            }

        }

        public void ProcessOldEvents()
        {
            var events = _dbs.EventDb.GetOldEvents(0);
            foreach (var @event in events)
            {
                var timer = new Stopwatch();
                timer.Start();
                _eventProcessor(@event);
                timer.Stop();
                //Console.WriteLine("One event processed - {0}", timer.ElapsedMilliseconds);
            }
        }



        public void StopMonitorEvents()
        {
            _monitorDb = false;
        }

        public void ProcessNewEvents(List<Event> events)
        {
            foreach (var @event in events)
            {
                _eventProcessor(@event);
                //Task.Factory.StartNew(() => _eventProcessor(@event), new CancellationToken());
            }
        }
    }
}
