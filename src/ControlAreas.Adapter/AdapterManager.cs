﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlAreas.Adapter.Services;
using ControlAreas.Dbs.Entities.DbEntities;

namespace ControlAreas.Adapter
{
    public class AdapterManager
    {
        public AdapterManager(ViFlowContext db)
        {
            RecognitionService = new RecognitionService(db);
            EventProvider = new EventProvider(db);
            VehicleInfoProvider = new VehicleInfoProvider(db);
        }

        public AdapterManager(ViFlowContext db, EventProvider provider)
        {
            RecognitionService = new RecognitionService(db);
            EventProvider = provider;
            VehicleInfoProvider = new VehicleInfoProvider(db);
        }

        public void Initialize()
        {
            
        }

        public RecognitionService RecognitionService { get; protected set; }
        public EventProvider EventProvider { get; protected set; }
        public VehicleInfoProvider VehicleInfoProvider { get; protected set; }
    }
}
